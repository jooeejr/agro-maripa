<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstoqueLocalidade extends Model
{
    //
    protected $fillable = ['local', 'produto_id', 'quantidade'];


    function produtos(){
        return $this->belongsTo('App\Produto', 'produto_id');
    }
}
