<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fornecedor extends Model
{
    // Define a tabela a qual o model se refere
    protected $table = "fornecedores";
}
