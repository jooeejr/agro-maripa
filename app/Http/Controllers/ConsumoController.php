<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lancamento;

class ConsumoController extends Controller
{
    /**
     * Função que retorna  a view
     */

    public function index()
    {
        /**
         * Modelo de lançamentos
        */
        $localidades = Lancamento::select('localidade')->groupBy('localidade')->get();
        return view('consumo', compact('localidades'));
    }


    /**
     * Consulta o consumo animal de um determinado periodo
     */
    public function getConsumo(Request $request)
    {
        /*Nomeia os requests em variaveis*/
        $localidade = $request->localidade;
        $lote = $request->lote;
        $de = $request->de;
        $ate = $request->ate;

        $previsto = Lancamento::where('localidade', $localidade)
            ->whereDate('created_at', '>=', $de)
            ->whereDate('created_at', '<=', $ate)
            ->sum('previsto');

        $realizado = Lancamento::where('localidade', $localidade)
            ->whereDate('created_at', '>=', $de)
            ->whereDate('created_at', '<=', $ate)
            ->sum('realizado');

        return compact('previsto', 'realizado');
    }
}
