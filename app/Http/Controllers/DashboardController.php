<?php

namespace App\Http\Controllers;

use App\Estoque;
use App\EstoqueLocalidade;
use App\Fornecedor;
use Illuminate\Http\Request;
use App\Produto;
use App\Lote;
use App\LancamentoProduto;

class DashboardController extends Controller
{
    function __construct()
    {
        return $this->middleware('auth');
    }


    // Gráfico com o estoque dos  produtos
    public function index(){

        // Produtos
        $produtos = Produto::with('estoque')->get();

        // Localidades
        $localidades = Lote::select('local')->groupBy('local')->get();

        // Total Gado
        $gado = Lote::all()->count();

        // Lotes
        $lotes = Lote::select('lote_reprodutivo')
            ->groupBy('lote_reprodutivo')
            ->count();

        // Maior Peso
        $locais = Lote::select('local')
            ->groupBy('local')
            ->count();

        // Fornecedores
        $fornecedores = Fornecedor::all()->count();

        return view('dashboard', compact(
            [
                'produtos',
                'localidades',
                'gado',
                'lotes',
                'fornecedores',
                'locais'
            ]
        ));

    }

    public function getDashboardFilter(Request $request){
       // Local
        $local = $request->local;

        // Seleciona o total de gado da localidade
        $gado = Lote::all()->where('local', $local)->count();

        // Lotes de uma localidade
        $lotes = Lote::select('lote_reprodutivo')
            ->where('local', $local)
            ->groupBy('lote_reprodutivo')
            ->count();

        /**
         * Seleciona os produtos de cada localidade
         * Seleciona o estoque de cada produto
         */

        $produtos = EstoqueLocalidade::with('produtos')
            ->where('local', $local)
            ->get();



        return compact('gado', 'lotes', 'produtos');
    }
}
