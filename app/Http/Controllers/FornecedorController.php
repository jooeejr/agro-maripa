<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Fornecedor;

class FornecedorController extends Controller
{
    function __construct()
    {
        return $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Lista de Fornecedores
        $fornecedores = Fornecedor::all();
        return view('fornecedores', compact('fornecedores'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Instância
        $fornecedor = new Fornecedor();

        // Seta os valores
        $fornecedor->fornecedor = $request->input('fornecedor');
        $fornecedor->save();

        // Redireciona
        return redirect('/fornecedores');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // Dados do Fornecedor
        $fornecedor = Fornecedor::find($id);

        if($fornecedor)
        {
            return view('fornecedores-edit', compact('fornecedor'));
        }

        return redirect('/fornecedores');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Seleciona o fornecedor que será atualizado
        $fornecedor = Fornecedor::find($id);

        // Se encontrar o fornecedor, atualiza o mesmo
        if ($fornecedor)
        {
            $fornecedor->fornecedor = $request->input('fornecedor');
            $fornecedor->save();

            return redirect('/fornecedores');
        }

        // Caso não ache o fornecedor, volta para tela de cadastro
        return redirect('/fornecedores');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Seleciona o fornecedor
        Fornecedor::destroy($id);

        // Se nao encontrar o ID apenas redireciona
        return response(200);
    }
}
