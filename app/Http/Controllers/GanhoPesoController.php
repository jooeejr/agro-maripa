<?php

namespace App\Http\Controllers;

use App\Lote;
use Illuminate\Http\Request;
use App\Racao;

class GanhoPesoController extends Controller
{
    // Faz a prévia do ganho de peso
    public function previa()
    {
        $racoes = Racao::all();
        $localidade = Lote::select('local')->groupBy('local')->orderBy('local', 'asc')->get();
        return view('peso', compact('racoes', 'localidade'));
    }

}
