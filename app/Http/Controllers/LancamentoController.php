<?php

namespace App\Http\Controllers;

use App\Lancamento;
use App\RacaoLancamento;
use Illuminate\Http\Request;
use App\Lote;
use App\Racao;
use App\LancamentoProduto;
use Yajra\DataTables\DataTables;
use App\Estoque;
use App\EstoqueLocalidade;

class LancamentoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Localidades
        $localidades = Lote::select('local')
            ->groupBy('local')
            ->orderBy('local', 'asc')
            ->get();

        // Racoes cadastradas
        $racoes = Racao::all();

        // Retorna a view
        return view('lancamentos', compact([
            'localidades',
            'racoes'
        ]));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /**
         * Verifica se há estoque suficiente para fazer o lançamento
         */

        // Adiciona o horario no created at do formulario
        $hour = date('H:i:s');
        $created_at = $request->input('created_at').' '.$hour;


        // Lançamento
        $lancamento = new Lancamento();

        $lancamento->localidade = $request->input('localidade');
        $lancamento->grupo = $request->input('grupo');
        $lancamento->fazenda = $request->input('fazenda');
        $lancamento->peso_medio = $request->input('peso_medio');
        $lancamento->total_animais = $request->input('total_animais');
        $lancamento->racao_id = $request->input('racao');
        $lancamento->leitura = $request->input('leitura');
        $lancamento->previsto = $request->input('previsto');
        $lancamento->realizado = $request->input('realizado');
        $lancamento->lembrete = $request->input('lembrete');
        $lancamento->created_at = $created_at;

        $lancamento->save();


        /**
         * @var $racao = Seleciona todos os produtos de uma determinada ração, para dar baixa no estoque
         * @var $realizado = Seleciona o total de ração que está sendo lançado
         * Faz um looping para selecionar o total de cada produto que contém dentro da ração, subtrai o valor e
         * atualiza a tabela de estoque
         */
        $racao = RacaoLancamento::where('racao_id', $request->input('racao'))->get();
        $realizado = $request->input('realizado');

        foreach($racao AS $rac)
        {
            $debito = ($rac->quantidade * $realizado);
            // Debita do Estoque Geral
            Estoque::where('produto_id', $rac->produto_id)->decrement('quantidade', $debito);

            // Debita do Estoque da Localidade
            EstoqueLocalidade::where('produto_id', $rac->produto_id)
                ->where('local', $request->input('localidade'))
                ->decrement('quantidade', $debito);
        }

        // Redireciona
        return redirect('/lancamentos');

    }


    function dadosLancamentos()
    {
        $lancamentos = Lancamento::with('racoes')->orderBy('created_at', 'desc');

        return Datatables::of($lancamentos)
            ->addColumn('racao_id', function ($data){
                return $data->racoes->racao;
            })
            ->editColumn('leitura', function($leitura){
                if($leitura->leitura == '0'){
                    return 'Nada';
                }

                if($leitura->leitura == '1'){
                    return 'Pouco';
                }

                if($leitura->leitura == '2'){
                    return 'Cheio';
                }

            })
            ->make(true);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
