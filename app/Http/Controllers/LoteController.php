<?php

namespace App\Http\Controllers;

use App\Lancamento;
use Illuminate\Http\Request;
use App\Lote;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;

class LoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Lotes
        return view('lotes');
    }

    public function dadosLotes()
    {
        $lotes = Lote::select(['id', 'apelido', 'sexo', 'data_nascimento', 'data_peso' ,'peso', 'local', 'lote_reprodutivo', 'grupo']);

        return Datatables::of($lotes)->make('true');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


    }

    public function tirarAcentos($string){
        $LetraProibi = Array("'","\"","¨","*","^","~","?","%");

        $special = Array('Á','È','ô','Ç','á','è','Ò','ç','Â','Ë','ò','â','ë','Ø','Ñ','À','Ð','ø','ñ','à','ð','Õ','Å','õ','Ý','å','Í','Ö','ý','Ã','í','ö','ã',
            'Î','Ä','î','Ú','ä','Ì','ú','Æ','ì','Û','æ','Ï','û','ï','Ù','®','É','ù','©','é','Ó','Ü','Þ','Ê','ó','ü','þ','ê','Ô','ß','‘','’','‚','“','”','„');

        $clearspc = Array('a','e','o','c','a','e','o','c','a','e','o','a','e','o','n','a','d','o','n','a','o','o','a','o','y','a','i','o','y','a','i','o','a',
            'i','a','i','u','a','i','u','a','i','u','a','i','u','i','u','','e','u','c','e','o','u','p','e','o','u','b','e','o','b','','','','','','');
        $newId = str_replace($special, $clearspc, $string);
        $newId = str_replace($LetraProibi, "", trim($newId));
        return strtoupper($newId);
    }


    public function uploadFile(Request $request){

        if ($request->input('send') != null ){

            $file = $request->file('lote');

            // File Details
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $tempPath = $file->getRealPath();
            $fileSize = $file->getSize();
            $mimeType = $file->getMimeType();

            // Valid File Extensions
            $valid_extension = array("csv");

            // 2MB in Bytes
            $maxFileSize = 20097152;

            // Check file extension
            if(in_array(strtolower($extension),$valid_extension)){

                // Check file size
                if($fileSize <= $maxFileSize){

                    // File upload location
                    $location = 'uploads';

                    // Upload file
                    $file->move($location,$filename);

                    // Import CSV to Database
                    $filepath = public_path($location."/".$filename);

                    // Reading file
                    $file = fopen($filepath,"r");

                    $importData_arr = array();
                    $i = 0;

                    while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
                        $num = count($filedata );
                        // Skip first row (Remove below comment if you want to skip the first row)
                        /*if($i == 0){
                           $i++;
                           continue;
                        }*/
                        for ($c=0; $c < $num; $c++) {
                            $importData_arr[$i] = $filedata [$c];
                        }
                        $i++;
                    }
                    fclose($file);


                    // esvazia a tabela
                    DB::table('lotes')->truncate();

                    // Insert to MySQL database
                    foreach($importData_arr as $key => $importData){
                        // Pula a primeira linha
                        if($key == 0)
                        {
                            continue;
                        }

                        $sep = explode(";", $importData);
                        $insertData = array(
                            "apelido"=> $sep[0],
                            "sexo"=> ($sep[1] == '') ? null : $sep[1],
                            "data_nascimento"=> ($sep[2] == '') ? null : $sep[2],
                            "local"=> ($sep[3] == '') ? null : $this->tirarAcentos(utf8_encode($sep[3])),
                            "peso"=> ($sep[4] == '') ? null : $sep[4],
                            "data_peso"=> ($sep[5] == '') ? null : $sep[5],
                            "lote_reprodutivo"=> ($sep[6] == '') ? null : $this->tirarAcentos(utf8_encode($sep[6])),
                            "grupo"=> ($sep[7] == '') ? null : $this->tirarAcentos(utf8_encode($sep[7])),
                            "fazenda"=> ($sep[8] == '') ? null : $this->tirarAcentos(utf8_encode($sep[8])),
                            "created_at" => DB::raw('CURRENT_TIMESTAMP'),
                            "updated_at" => DB::raw('CURRENT_TIMESTAMP')
                        );
                        Lote::insertData($insertData);

                    }

                    /*Session::flash('message','Import Successful.');*/
                }else{
                    /*Session::flash('message','File too large. File must be less than 2MB.');*/
                }

            }else{
                /*Session::flash('message','Invalid File Extension.');*/
            }

        }

        // Redirect to index
        return redirect()->action('LoteController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // Dados do lote
        $lote = Lote::find($id);

        // Se o lote existe exibe os dados
        if($lote)
        {
            return view('lotes-edit', compact('lote'));
        }


        // redireciona caso nao encontre o lote
        return redirect('/lotes');

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Seleciona o Lote que será atualizado
        $lote = Lote::find($id);

        if($lote)
        {
            // Seta os dados e salva no banco
            $lote->lote = $request->input('lote');
            $lote->save();
        }


        // Redireciona para a pagina dos lotes
        return redirect('/lotes');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Seleciona e deleta o lote
        Lote::destroy($id);

        return response('200');
    }



    // Busca todos os lotes de um determinado local
    public function getLote(Request $request)
    {
        // Post
        $local = $request->local;

        // Fazendas
        $fazenda = Lote::select('fazenda')->where('local', $local)->first();

        // Grupos
        $grupos = Lote::select('grupo')
            ->where('local', $local)
            ->groupBy('grupo')
            ->get();

        $lotes = Lote::select('lote_reprodutivo')
            ->where('local', $local)
            ->groupBy('lote_reprodutivo')
            ->orderBy('lote_reprodutivo', 'asc')
            ->get();

        return compact('lotes', 'fazenda', 'grupos');
    }


    // Busca as fazendas de um determinado grupo de lote
    public function getFazenda(Request $request){
        $grupo = $request->grupo;

        $fazendas = Lote::select('fazenda')
            ->where('grupo', $grupo)
            ->groupBy('fazenda')
            ->get();

        return $fazendas;
    }

    // Busca todos os lotes de um determinado local
    public function getLoteConsumo(Request $request)
    {
        // Post
        $local = $request->local;

        $lotes = Lancamento::select('lote')
            ->where('localidade', $local)
            ->groupBy('lote')
            ->orderBy('lote', 'asc')
            ->get();

        return $lotes;
    }


    // Busca a média de peso de um determinado lote
    public function getMedia(Request $request)
    {
        // Post
        $localidade = $request->localidade;
        $grupo = $request->grupo;

        $media = Lote::select('peso')
            ->where('local', $localidade)
            ->where('grupo', $grupo)
            ->avg('peso');

        // Número formatado para rertono
        $format = number_format($media,2);

        // Total de animais
        $total = Lote::where('local', $localidade)
            ->where('grupo', $grupo)
            ->count();


        // Retorna o valor e o total de animais
        return compact('format', 'total');
    }

    // Busca a média de peso de um determinado lote
    public function getMediaGrupo(Request $request)
    {
        // Post
        $grupo = $request->grupo;
        $fazenda = $request->fazenda;

        $media = Lote::select('peso')
            ->where('grupo', $grupo)
            ->where('fazenda', $fazenda)
            ->avg('peso');

        // Número formatado para rertono
        $format = number_format($media,2);


        // Retorna o valor e o total de animais
        return compact('format');
    }
}
