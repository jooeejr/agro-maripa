<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Fornecedor;
use App\Produto;
use App\LancamentoProduto;
use App\Estoque;
use App\Lote;
use App\EstoqueLocalidade;

class ProdutoController extends Controller
{
    function __construct()
    {
        return $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Retorna os fornecedores e os produtos para a view
        $fornecedores = Fornecedor::all();
        $produtos = Produto::all();

        return view('produtos', compact('fornecedores', 'produtos'));
    }


    // Formulario de lançamento
    public function indexLancamento($id)
    {
        // Encontra o produto
        $produto = Produto::find($id);

        // Lista todos os forncedores
        $fornecedores = Fornecedor::all();

        // Localidades
        $localidades = Lote::select('local')
            ->groupBy('local')
            ->orderBy('local', 'asc')
            ->get();

        // Lançamentos
        $lancamentos = LancamentoProduto::where('produto_id', $id)->get();

        // Verifica se o produto existe
        if ($produto)
        {
            return view('produtos-lancamento', compact([
                'produto',
                'fornecedores',
                'lancamentos',
                'localidades'
            ]));
        }

        // Redireciona caso nao encontre o produto
        return redirect('/produtos');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // instância da classe
        $produtos = new Produto();

        // Seta os valores e salva no banco
        $produtos->produto = $request->input('produto');

        // Estoque
        $estoque = new Estoque();

        // Cadastra o produto e tambem no estoque
        $produtos->save();

        $estoque->produto_id = $produtos->id;
        $estoque->save();

        // redirect
        return redirect('/produtos');
    }



    // Cadastro de um novo lançamento
    public function storeLancamento(Request $request, $id)
    {
        $msg = [
            'preco.required' => 'O campo preço é obrigatório !',
            'qtd.required' => 'O campo quantidade é obrigatório',
            'fornecedor.required' => 'O campo Fornecedor é obrigatório',
            'nf.required' => 'O campo nota fiscal é obrigatório',
            'nf.unique' => 'Nota fiscal já existe !',
            'localidade.unique' => 'O campo local é obrigatório !'
        ];

        // Validação dos campos
        $request->validate([
            'preco' => 'required',
            'qtd' => 'required',
            'fornecedor' => 'required',
            'nf' => 'required|unique:lancamento_produtos,nf',
            'localidade' => 'required|string'
        ], $msg);

        // instância da classe
        $lancamento = new LancamentoProduto();

        // Seta os valores e salva no banco
        $lancamento->produto_id = $id;
        $lancamento->preco = $request->input('preco');
        $lancamento->quantidade = $request->input('qtd');
        $lancamento->fornecedor_id = $request->input('fornecedor');
        $lancamento->nf = $request->input('nf');
        $lancamento->local = $request->input('localidade');
        $lancamento->save();

        // Atualiza o estoque
        $qtd_estoque = Estoque::where('produto_id', $id)->increment('quantidade',$request->input('qtd'));

        // Da entrada no estoque por localidade
        $estoqueLoc = EstoqueLocalidade::where('local', $request->input('localidade'))
            ->where('produto_id', $id)
            ->count();

        if($estoqueLoc == 0){
            EstoqueLocalidade::create([
                'local' => $request->input('localidade'),
                'produto_id' => $id,
                'quantidade' => $request->input('qtd')
            ]);
        }else{
            EstoqueLocalidade::where('local', $request->input('localidade'))
                ->where('produto_id', $id)
                ->increment('quantidade',$request->input('qtd'));
        }

        // redirect
        return redirect('/lancamento-produto/'.$id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // Dados do Produto
        $produto = Produto::find($id);

        // Fornecedores
        $fornecedores = Fornecedor::all();

        if($produto)
        {
            return view('produtos-edit', compact('produto', 'fornecedores'));
        }

        return redirect('/produtos');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // instância da classe
        $produto = Produto::find($id);


        if($produto)
        {
            // Seta os valores e salva no banco
            $produto->produto = $request->input('produto');
            $produto->save();

            return redirect('/produtos');
        }

        // redirect
        return redirect('/produtos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Delete do produto e de seus lancamentos
        $produto = Produto::find($id);

        // Deleta os lançamentos od produto
        $produto->lancamentos()->delete();

        // Deleta os produtos dentro uma determinada ração
        $produto->produtosRacoes()->delete();

        // Deleta o produto do estoque
        $produto->estoque()->delete();

        // Deleta o produto
        $produto->delete();

        return response('200');
    }
}
