<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Racao;
use App\Produto;
use App\RacaoLancamento;

class RacaoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Retorna as racoes
        $racao = Racao::all();

        return view('racoes', compact('racao'));
    }

    // Formulario de montagem da racao
    public function indexMontar($id)
    {
        // Racao
        $racao = Racao::find($id);

        // Produtos
        $produtos = Produto::all();

        // Produtos Relacionados a ração
        $produtosRe = RacaoLancamento::where('racao_id', $id)->get();


        // Retorna os fornecedores e os produtos para a view
        return view('racoes-montagem', compact('racao', 'produtos', 'produtosRe'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Instancia da classe
        $racao = new Racao();

        $racao->racao = $request->input('racao');
        $racao->quantidade = $request->input('qtd');
        $racao->save();

        return redirect('/racoes');
    }


    public function storeMontar(Request $request, $id)
    {
        for ($i = 0; $i < count($request->produto); $i++)
        {
            RacaoLancamento::create([
                'racao_id' => $id,
                'produto_id' => $request->produto[$i] ,
                'quantidade' => $request->qtd_produto[$i]
            ]);
        }

        return redirect('/montar-racao/'.$id);
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // Retorna os dados de uma racao
        $racao = Racao::find($id);

        if($racao)
        {
            return view('racoes-edit', compact('racao'));
        }

        // redireciona caso nao encontre o dado acima
        return redirect('/racoes');

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Atualiza uma determinada racao
        $racao = Racao::find($id);

        // Seta as novas propriedades
        $racao->racao = $request->input('racao');
        $racao->save();

        // Redireciona para a página de racoes
        return redirect('/racoes');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Deleta uma ração
        $racao = Racao::find($id);
        $racao->racaoLancamentos()->delete();
        $racao->delete();

        return response('200');
    }


    public function destroyMontar($id)
    {
        // Deleta o produto de dentro da ração
        $produtos = RacaoLancamento::find($id);
        $produtos->delete();


        // Retorna para a pagina
        return response('200');
    }



}
