<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lote;
use App\Produto;

class RotaController extends Controller
{
    /**
     * Retorna os grupos agrupados
     */
    function index(){
        $grupos = Lote::select('grupo')
            ->groupBy('grupo')
            ->get();

        $produtos = Produto::all();

        return view('rotas', compact('grupos', 'produtos'));
    }

    // Gera a media de cada localidade e calcula a porcentagem informada
    function getRelatorio(Request $request){
        $grupo = $request->grupo;
        $fazenda = $request->fazenda;
        $percent = $request->percent;
        $realizado = $request->realizado;
        $deposito = $request->deposito;
        $produto = $request->produto;

        // Localidades do grupo x fazenda
        $rotas = Lote::select('local')
            ->where('grupo', $grupo)
            ->where('fazenda', $fazenda)
            ->groupBy('local')
            ->get();


        $data = array();

        // Retorno do array
        $cont = 0;
        foreach($rotas AS $rota){
            $avg = Lote::select('local')
                    ->where('local', $rota->local)
                    ->avg('peso');

            // Animais
            $animais = Lote::where('local', $rota->local)
                ->count();



            // Calcula a porcentagem de cada media
            $calcPercent = ($avg / 100) * $percent;

           // Media x Animais
            $mediaxanimal = ($animais *  $calcPercent);

            $data[$cont] =
                [
                    'local' => $rota->local,
                    'media' => number_format($avg,2),
                    'porcentagem' => $percent,
                    'perc_media' => number_format($calcPercent, 2),
                    'realizado' => ($realizado == null || $realizado == '0.00') ? '' : $realizado,
                    'produto' => $produto,
                    'deposito' => ($deposito == null || $realizado == '0.00') ? '' : $deposito,
                    'mediaxanimal' => number_format($mediaxanimal, 2)
                ];

            $cont++;
        }

        return $data;


     }

}
