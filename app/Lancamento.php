<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lancamento extends Model
{
    // Ração
    public function racoes()
    {
        return $this->belongsTo('App\Racao', 'racao_id');
    }
}
