<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LancamentoProduto extends Model
{
    function fornecedor()
    {
        return $this->belongsTo('App\Fornecedor');
    }

    function produtos()
    {
        return $this->belongsTo('App\Produto', 'produto_id');
    }
}
