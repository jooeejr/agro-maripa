<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Lote extends Model
{
    // Timestamp
    public $timestamps = true;

    // Campos que receberão inserts
    public $fillable = ['apelido', 'sexo', 'data_nascimento', 'local', 'peso', 'data_peso',
        'lote_reprodutivo'];

    public static function insertData($data){
        $value=DB::table('lotes')->where('apelido', $data['apelido'])->get();

        if($value->count() == 0){
            DB::table('lotes')->insert($data);
        }
    }
}
