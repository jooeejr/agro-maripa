<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produto extends Model
{
    function lancamentos(){
        return $this->hasMany('App\LancamentoProduto');
    }

    function produtosRacoes(){
        return $this->hasMany('App\RacaoLancamento');
    }

    function estoque()
    {
        return $this->hasOne('App\Estoque');
    }
}
