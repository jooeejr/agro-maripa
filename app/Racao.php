<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Racao extends Model
{
    // Seta a tabela que está em uso
    protected $table = "racoes";

    // Relacionamento
    public function racaoLancamentos()
    {
        return $this->hasMany('App\RacaoLancamento');
    }
}
