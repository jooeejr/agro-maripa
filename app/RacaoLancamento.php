<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RacaoLancamento extends Model
{
    // Relation
    public function produto()
    {
        return $this->belongsTo('App\Produto');
    }


    // Columns
    protected $fillable = ['racao_id','produto_id','quantidade'];


}
