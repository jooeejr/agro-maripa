<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lotes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('apelido')->unique();
            $table->string('sexo')->nullable();
            $table->integer('data_nascimento')->nullable();
            $table->string('local')->nullable();
            $table->decimal('peso', '10', '2')->nullable();
            $table->integer('data_peso')->nullable();
            $table->string('lote_reprodutivo', '300')->nullable();
            $table->string('grupo', '200');
            $table->string('fazenda', '200');
            $table->timestamps();
        });

        // LOTE ABAIXO NÃO CONVÉM PARA O CLIENTE
//        Schema::create('lotes', function (Blueprint $table) {
//            $table->increments('id');
//            $table->string('lote');
//            $table->integer('localidade_id')->unsigned();
//            $table->timestamps();
//
//            // Relation
//            $table->foreign('localidade_id')
//                ->references('id')
//                ->on('localidades');
//        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lotes');
    }
}
