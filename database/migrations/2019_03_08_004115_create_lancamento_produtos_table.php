<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLancamentoProdutosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lancamento_produtos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('produto_id');
            $table->integer('fornecedor_id');
            $table->decimal('preco', 10,2);
            $table->decimal('quantidade', 10,2);
            $table->string('nf')->unique();
            $table->string('local');
            $table->timestamps();

            //Relations
            $table->foreign('fornecedor_id')
                ->references('id')
                ->on('fornecedores');

            $table->foreign('produto_id')
                ->references('id')
                ->on('produtos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lancamento_produtos');
    }
}
