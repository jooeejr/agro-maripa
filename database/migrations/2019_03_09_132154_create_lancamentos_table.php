<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLancamentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lancamentos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('localidade');
            $table->string('grupo');
            $table->string('fazenda');
            $table->decimal('peso_medio', '10', '2');
            $table->integer('total_animais')->unsigned();
            $table->integer('racao_id');
            $table->integer('leitura');
            $table->decimal('previsto', '10', '2');
            $table->decimal('realizado', '10', '2');
            $table->date('lembrete')->nullable();
            $table->timestamps();

            $table->foreign('racao_id')->references('id')->on('racoes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lancamentos');
    }
}
