<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRacaoLancamentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('racao_lancamentos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('racao_id')->unsigned();
            $table->integer('produto_id')->unsigned();
            $table->decimal('quantidade', '10', 2);
            $table->timestamps();

            $table->foreign('racao_id')
                ->references('racao')
                ->on('id');

            $table->foreign('produto_id')
                ->references('produtos')
                ->on('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('racao_lancamentos');
    }
}
