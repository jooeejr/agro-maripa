<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(FornecedorSeeder::class);
//         $this->call(ProdutoSeeder::class);
//         $this->call(LancamentoProdutoSeeder::class);
//         $this->call(RacaoSeeder::class);
         $this->call(UserSeeder::class);
//        $this->call(EstoqueSeeder::class);
    }
}
