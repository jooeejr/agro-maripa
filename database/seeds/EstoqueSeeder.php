<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EstoqueSeeder extends Seeder
{
    /**
     * Run the database seeds.
     * Cadastro do estoque de produtos
     * @return void
     */
    public function run()
    {

        DB::table('estoque')->insert(
            [
                'produto_id' => '1',
                'quantidade' => '200.00',
            ]
        );

        DB::table('estoque')->insert(
            [
                'produto_id' => '2',
                'quantidade' => '150.45',
            ]
        );

    }
}
