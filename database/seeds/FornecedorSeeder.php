<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FornecedorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Insert de Fornecedores
        DB::table('fornecedores')->insert(
            [
                'fornecedor' => 'Fornecedor 1'
            ]
        );
        DB::table('fornecedores')->insert(
            [
                'fornecedor' => 'Fornecedor 2'
            ]
        );
    }
}
