<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LancamentoProdutoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Insert de Lançamentos de Produtos
        DB::table('lancamento_produtos')->insert(
            [
                'produto_id' => '1',
                'fornecedor_id' => '1',
                'preco' => '10.99',
                'quantidade' => '200.00',
                'nf' => md5(uniqid()),
                'local' => 'P 01 SEDE',
            ]
        );

        DB::table('lancamento_produtos')->insert(
            [
                'produto_id' => '2',
                'fornecedor_id' => '2',
                'preco' => '10.99',
                'quantidade' => '150.45',
                'nf' => md5(uniqid()),
                'local' => 'P 32 CASTANHEIRA',
            ]
        );
    }
}
