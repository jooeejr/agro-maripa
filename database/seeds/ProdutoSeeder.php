<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProdutoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Insert de Produtos
        DB::table('produtos')->insert(
            [
                'produto' => 'Milho'
            ]
        );
        DB::table('produtos')->insert(
            [
                'produto' => 'Farelo de Soja'
            ]
        );
    }
}
