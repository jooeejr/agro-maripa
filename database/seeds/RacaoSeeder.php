<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RacaoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Insert de Ração
        DB::table('racoes')->insert(
            [
                'racao' => '30 M',
                'quantidade' => '1.00',
            ]
        );


        // Lançamento da Ração
        DB::table('racao_lancamentos')->insert(
            [
                'racao_id' => '1',
                'produto_id' => '1',
                'quantidade' => '0.50',
            ]
        );

        DB::table('racao_lancamentos')->insert(
            [
                'racao_id' => '1',
                'produto_id' => '2',
                'quantidade' => '0.50',
            ]
        );
    }
}
