<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Insert de Usuario
        DB::table('users')->insert(
            [
                'name' => 'Admin',
                'email' => 'teste@teste.com',
                'password' => Hash::make('123'),
            ]
        );

    }
}
