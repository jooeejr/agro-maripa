// Cadastra um novo fornecedor
function cadFornecedor()
{
    // Verifica se os campos estão preenchidos
    var fornecedor = $("#fornecedor").val();
    if(fornecedor == '')
    {
        swal({
           title: 'Preencha o(s) campo(s) corretamente!',
           icon: 'warning'
        });
    }else{
        // Modal de Aguarde
        swal({
            text: 'Aguarde...',
            closeModal: false,
            buttons: false,
            closeOnClickOutside: false
        });

        // Envia o Formulario
        $("#formFornecedor").submit();
    }
}



// Atualização de fornecedor
function attFornecedor()
{
    // Verifica se os campos estão preenchidos
    var fornecedor = $("#fornecedor").val();
    if(fornecedor == '')
    {
        swal({
            title: 'Preencha o(s) campo(s) corretamente!',
            icon: 'warning'
        });
    }else{
        // Modal de Aguarde
        swal({
            text: 'Aguarde...',
            closeModal: false,
            buttons: false,
            closeOnClickOutside: false
        });

        // Envia o Formulario
        $("#formEditFornecedor").submit();
    }

}



// remoção de fornecedor
function removeFornecedor(id)
{
    // Confirmação de remoção do fornecedor
    swal({
        title: 'Deletar Fornecedor ?',
        text: 'Ao deletar o fornecedor, produtos associados ao mesmo, serão afetados.',
        icon: 'warning',
        buttons: ['Não, cancele por favor !', 'Sim, quero deletar !']
    }).then((willDelete) => {
        if (willDelete) {

            // Loading
            swal({
                text: 'Aguarde...',
                closeModal: false,
                buttons: false,
                closeOnClickOutside: false
            });


            // Ajax
            $.ajax({
                url: '/fornecedores/'+id,
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {"_method": "delete"},
                success: function(data){
                    if(data == 200)
                    {
                        // Redireciona
                        window.location.href='/fornecedores';
                    }
                },
                error: function(data){
                    swal({
                       title: 'Oooops !',
                       text: 'Tente Novamente !',
                       icon: 'error'
                    });
                }
            });
        }
    });
}



// Cadastra um novo produto
function cadProduto()
{
    // Verifica se os campos estão preenchidos
    var produto = $("#produto").val();
    var preco = $("#preco").val();
    var fornecedor = $("#fornecedor").val();
    var nf = $("#nf").val();

    if(produto == '' || preco == "" || fornecedor == "" || nf == "")
    {
        swal({
            title: 'Preencha o(s) campo(s) corretamente!',
            icon: 'warning'
        });
    }else{
        // Modal de Aguarde
        swal({
            text: 'Aguarde...',
            closeModal: false,
            buttons: false,
            closeOnClickOutside: false
        });

        // Envia o Formulario
        $("#formProduto").submit();
    }
}


// Atualização de Produto
function attProduto()
{
    // Verifica se os campos estão preenchidos
    var produto = $("#produto").val();
    var preco = $("#preco").val();
    var fornecedor = $("#fornecedor").val();
    var nf = $("#nf").val();

    if(produto == '' || preco == "" || fornecedor == "" || nf == "")
    {
        swal({
            title: 'Preencha o(s) campo(s) corretamente!',
            icon: 'warning'
        });
    }else{
        // Modal de Aguarde
        swal({
            text: 'Aguarde...',
            closeModal: false,
            buttons: false,
            closeOnClickOutside: false
        });

        // Envia o Formulario
        $("#formEditProduto").submit();
    }

}



// remoção de fornecedor
function removeProduto(id)
{
    // Confirmação de remoção do fornecedor
    swal({
        title: 'Deletar Produto ?',
        icon: 'warning',
        buttons: ['Não, cancele por favor !', 'Sim, quero deletar !']
    }).then((willDelete) => {
        if (willDelete) {

            // Loading
            swal({
                text: 'Aguarde...',
                closeModal: false,
                buttons: false,
                closeOnClickOutside: false
            });


            // Ajax
            $.ajax({
                url: '/produtos/'+id,
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {"_method": "delete"},
                success: function(data){
                    if(data == 200)
                    {
                        // Redireciona
                        window.location.href='/produtos';
                    }
                },
                error: function(data){
                    swal({
                        title: 'Oooops !',
                        text: 'Tente Novamente !',
                        icon: 'error'
                    });
                }
            });
        }
    });
}



// Cadastra um novo fornecedor
function cadLote()
{
    // Verifica se os campos estão preenchidos
    var lote = $("#lote").val();
    if(lote == '')
    {
        swal({
            title: 'Preencha o(s) campo(s) corretamente!',
            icon: 'warning'
        });
    }else{
        // Modal de Aguarde
        swal({
            text: 'Aguarde...',
            closeModal: false,
            buttons: false,
            closeOnClickOutside: false
        });

        // Envia o Formulario
        $("#formLote").submit();
    }
}


// Atualização de fornecedor
function attLote()
{
    // Verifica se os campos estão preenchidos
    var lote = $("#lote").val();
    if(lote == '')
    {
        swal({
            title: 'Preencha o(s) campo(s) corretamente!',
            icon: 'warning'
        });
    }else{
        // Modal de Aguarde
        swal({
            text: 'Aguarde...',
            closeModal: false,
            buttons: false,
            closeOnClickOutside: false
        });

        // Envia o Formulario
        $("#formEditLote").submit();
    }

}



// remoção de fornecedor
function removeLote(id)
{
    // Confirmação de remoção do fornecedor
    swal({
        title: 'Deletar Lote ?',
        icon: 'warning',
        buttons: ['Não, cancele por favor !', 'Sim, quero deletar !']
    }).then((willDelete) => {
        if (willDelete) {

            // Loading
            swal({
                text: 'Aguarde...',
                closeModal: false,
                buttons: false,
                closeOnClickOutside: false
            });


            // Ajax
            $.ajax({
                url: '/lotes/'+id,
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {"_method": "delete"},
                success: function(data){
                    if(data == 200)
                    {
                        // Redireciona
                        window.location.href='/lotes';
                    }
                },
                error: function(data){
                    swal({
                        title: 'Oooops !',
                        text: 'Tente Novamente !',
                        icon: 'error'
                    });
                }
            });
        }
    });
}


// Cadastra um novo produto
function cadRacao()
{
    // Verifica se os campos estão preenchidos
    var racao = $("#racao").val();

    if(racao == '')
    {
        swal({
            title: 'Preencha o(s) campo(s) corretamente!',
            icon: 'warning'
        });
    }else{
        // Modal de Aguarde
        swal({
            text: 'Aguarde...',
            closeModal: false,
            buttons: false,
            closeOnClickOutside: false
        });

        // Envia o Formulario
        $("#formRacao").submit();
    }
}


// Atualização de Ração
function attRacao()
{
    // Verifica se os campos estão preenchidos
    var racao = $("#racao").val();

    if(racao == '')
    {
        swal({
            title: 'Preencha o(s) campo(s) corretamente!',
            icon: 'warning'
        });
    }else{
        // Modal de Aguarde
        swal({
            text: 'Aguarde...',
            closeModal: false,
            buttons: false,
            closeOnClickOutside: false
        });

        // Envia o Formulario
        $("#formEditRacao").submit();
    }

}


// remoção de ração
function removeRacao(id)
{
    // Confirmação de remoção do fornecedor
    swal({
        title: 'Deletar Ração ?',
        icon: 'warning',
        buttons: ['Não, cancele por favor !', 'Sim, quero deletar !']
    }).then((willDelete) => {
        if (willDelete) {

            // Loading
            swal({
                text: 'Aguarde...',
                closeModal: false,
                buttons: false,
                closeOnClickOutside: false
            });


            // Ajax
            $.ajax({
                url: '/racoes/'+id,
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {"_method": "delete"},
                success: function(data){
                    if(data == 200)
                    {
                        // Redireciona
                        window.location.href='/racoes';
                    }
                },
                error: function(data){
                    swal({
                        title: 'Oooops !',
                        text: 'Tente Novamente !',
                        icon: 'error'
                    });
                }
            });
        }
    });
}



// Cadastra a montagem de uma ração
function cadMontagem()
{

     var reqlength = $('.inpValidate').length;
     var value = $('.inpValidate').filter(function () {
         return this.value != '';
     });

     if (value.length>=0 && (value.length !== reqlength)) {
         swal({
             title: 'Preencha o(s) campo(s) corretamente!',
             icon: 'warning'
         });
     } else {
         // Loading
         swal({
             text: 'Aguarde...',
             closeModal: false,
             buttons: false,
             closeOnClickOutside: false
         });

         // Envia o Formulario
         $("#formMontagem").submit();
     }
}



// remoção de produto da ração
function removeProdutoRacao(id)
{
    // Confirmação de remoção do fornecedor
    swal({
        title: 'Deletar o produto da ração ?',
        icon: 'warning',
        buttons: ['Não, cancele por favor !', 'Sim, quero deletar !']
    }).then((willDelete) => {
        if (willDelete) {

            // Loading
            swal({
                text: 'Aguarde...',
                closeModal: false,
                buttons: false,
                closeOnClickOutside: false
            });


            // Ajax
            $.ajax({
                url: '/montar-racao/'+id,
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {"_method": "delete"},
                success: function(data){
                    if(data == 200)
                    {
                        // Redireciona
                        window.location.reload();
                    }
                },
                error: function(data){
                    swal({
                        title: 'Oooops !',
                        text: 'Tente Novamente !',
                        icon: 'error'
                    });
                }
            });
        }
    });
}

function logout()
{
    $.ajax({
        url: '/logout',
        type: 'POST',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(data){
            window.location.reload();
        }
    });
}



// Busca as fazendas pelo grupo de lote reprodutivo
function buscaFazenda(grupo){
    // Loading
    swal({
        text: 'Aguarde...',
        closeModal: false,
        buttons: false,
        closeOnClickOutside: false
    });


    // Ajax
    $.ajax({
        url: '/api/fazendas',
        type: 'POST',
        dataType: 'json',
        data: {'grupo': grupo},
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(data){
            // Option
            var option = "";

            // Limpa o campo
            $("#fazenda>option[class='adds']").remove();

            // Monta a estrutura para colocar no select
            for(i=0; i<data.length; i++)
            {
                option += "<option class='adds' value='"+data[i].fazenda+"'>"+data[i].fazenda+"</option>";
            }

            // Seta a fazenda
            $("#fazenda").append(option);

            // Remove o alert
            swal.close();

        },
        error: function(data){
            swal({
                title: 'Oooops !',
                text: 'Tente Novamente !',
                icon: 'error'
            });
        }
    });
}

// Busca de lotes pela localidade
function buscaLote(local)
{
    // Loading
    swal({
        text: 'Aguarde...',
        closeModal: false,
        buttons: false,
        closeOnClickOutside: false
    });


    // Ajax
    $.ajax({
        url: '/api/lotes',
        type: 'POST',
        dataType: 'json',
        data: {'local': local},
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(data){
            // Option
            var option = "";

            // Limpa o campo
            $("#fazenda>option[class='adds']").remove();
            $("#grupo>option[class='adds']").remove();

            // Seta a fazenda
            optionF = "<option selected class='adds' value='"+data['fazenda'].fazenda+"'>"+data['fazenda'].fazenda+"</option>";
            $("#fazenda").append(optionF);

            // Monta a estrutura para colocar no select
            for(i=0; i<data['grupos'].length; i++)
            {
                option += "<option class='adds' value='"+data['grupos'][i].grupo+"'>"+data['grupos'][i].grupo+"</option>";
            }

            // Coloca as options no select
            $("#grupo").append(option);

            // Remove o alert
            swal.close();

        },
        error: function(data){
            swal({
                title: 'Oooops !',
                text: 'Tente Novamente !',
                icon: 'error'
            });
        }
    });
}


function buscaLoteConsumo(local)
{
    // Loading
    swal({
        text: 'Aguarde...',
        closeModal: false,
        buttons: false,
        closeOnClickOutside: false
    });


    // Ajax
    $.ajax({
        url: '/api/lotes-consumo',
        type: 'POST',
        data: {'local': local},
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(data){

            // Option
            var option = "";

            // Limpa o campo
            $("#lote_reprodutivo>option[class='adds']").remove();


            // Monta a estrutura para colocar no select
            for(i=0; i<data.length; i++)
            {
                option += "<option class='adds' value='"+data[i]['lote']+"'>"+data[i]['lote']+"</option>";
            }

            // Coloca as options no select
            $("#lote_reprodutivo").append(option);

            // Remove o alert
            swal.close();
        },
        error: function(data){
            swal({
                title: 'Oooops !',
                text: 'Tente Novamente !',
                icon: 'error'
            });
        }
    });
}



// Calcula a média do peso daquele lote selecionado
function calculaMediaPeso(grupo)
{
    // Loading
    swal({
        text: 'Calculando média de peso...',
        closeModal: false,
        buttons: false,
        closeOnClickOutside: false
    });


    var localidade = $("#localidade").val();

    // Ajax
    $.ajax({
        url: '/api/peso-lotes',
        type: 'POST',
        data: {'localidade': localidade, 'grupo': grupo},
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(data){

            // Seta a media retornada no campo
            $("#peso_medio").val(data.format);

            // Seta o total de animais no cmapo
            $("#total_animais").val(data.total);

            // Remove o alert
            swal.close();
        },
        error: function(data){
            swal({
                title: 'Oooops !',
                text: 'Tente Novamente !',
                icon: 'error'
            });
        }
    });
}


function cadLancamento()
{
    // Verifica se os campos estão preenchidos
    var localidade = $("#localidade").val();
    var lotes = $("#lote").val();
    var grupo = $("#grupo").val();
    var peso_medio = $("#peso_medio").val();
    var total_animais = $("#total_animais").val();
    var racao = $("#racao").val();
    var leitura = $("#leitura").val();
    var previsto = $("#previsto").val();
    var realizado = $("#realizado").val();
    var data = $("#data").val();

    if( localidade == '' ||
        lotes == '' ||
        peso_medio == '' ||
        total_animais == '' ||
        racao == '' ||
        leitura == '' ||
        previsto == '' ||
        grupo == '' ||
        realizado == '' ||
        data == '')
    {
        swal({
            title: 'Preencha o(s) campo(s) corretamente!',
            icon: 'warning'
        });
    }else{
        // Modal de Aguarde
        swal({
            text: 'Aguarde...',
            closeModal: false,
            buttons: false,
            closeOnClickOutside: false
        });

        // Envia o Formulario
        $("#formLancamento").submit();
    }
}


function consumoAnimal()
{
    // Verifica se os campos estão preenchidos
    var localidade = $("#localidade").val();
    var lote_reprodutivo = $("#lote").val();
    var de = $("#de").val();
    var ate = $("#ate").val();

    if( localidade == '' ||
        lote_reprodutivo == '' ||
        de == '' ||
        ate == '')
    {
        swal({
            title: 'Preencha o(s) campo(s) corretamente!',
            icon: 'warning'
        });
    }else{
        swal({
            text: 'Aguarde...',
            closeModal: false,
            buttons: false,
            closeOnClickOutside: false
        });

        // Formulario
        var form = $("#formConsumo").serialize();

        // Ajax
        $.ajax({
            url: '/consumo',
            type: 'POST',
            data: form,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data){
                // Remove o alerta
                swal.close();


                var de = (data.previsto)+' KG';
                var ate = (data.realizado)+' KG';

                // Exibe os valores
                $("#consumoPrevisto").html(de);
                $("#consumoReal").html(ate);
            },
            error: function(data){
                swal({
                    title: 'Oooops !',
                    text: 'Tente Novamente !',
                    icon: 'error'
                });
            }
        });
    }
}


function ganhoDePeso()
{
    // Verifica se os campos estão preenchidos
    var racao = $("#racao").val();
    var de = $("#de").val();
    var ate = $("#ate").val();
    var ganho = $("#ganho").val();
    var local = $("#localidade").val();

    if( racao == '' ||
        de == '' ||
        ate == '' ||
        local == '' ||
        ganho == '')
    {
        swal({
            title: 'Preencha o(s) campo(s) corretamente!',
            icon: 'warning'
        });
    }else{
       // Localidade
        $("#localidadeRel").html(local);

        // Seta o nome da racao
        $("#racaoUtilizada").html(racao);

        // Seta a data de inicio
        var sepDe = de.split("-");
        $("#inicio").html(sepDe[2]+'/'+sepDe[1]+'/'+sepDe[0]);

        // Seta a data de fim
        var sepAte = ate.split("-");
        $("#fim").html(sepAte[2]+'/'+sepAte[1]+'/'+sepAte[0]);

        // Calcula o ganho de peso
        var date1 = new Date(de);
        var date2 = new Date(ate);
        var timeDiff = Math.abs(date2.getTime() - date1.getTime());
        var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
        var pesoTotal = (diffDays*ganho).toFixed(2);

        $("#pesoTotal").html(pesoTotal);

    }
}


function getMediaGrupo(fazenda){
    // Loading
    swal({
        text: 'Calculando média de peso...',
        closeModal: false,
        buttons: false,
        closeOnClickOutside: false
    });

    // Grupo
    var group = $("#grupo").val();


    // Ajax
    $.ajax({
        url: '/api/peso-grupo',
        type: 'POST',
        data: {'grupo': group, 'fazenda': fazenda},
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(data){

            // Seta a media retornada no campo
            $("#media").val(data.format);

            // Remove o alert
            swal.close();
        },
        error: function(data){
            swal({
                title: 'Oooops !',
                text: 'Tente Novamente !',
                icon: 'error'
            });
        }
    });
}


function calculaPercent(pc){
    // Média
    var media = $("#media").val();

    // Calcula a porcentagem da media, baseado no valor informado
    var result = (media / 100) * pc;
    result = result.toFixed(2);

    // Coloca o resulado no campo
    $("#result_percent").val(result);

}



// Seta os valores nos campos do relatório
function geraRelatorioRota(){
    // Dados do form
    var grupo = $("#grupo").val();
    var fazenda = $("#fazenda").val();
    var percent = $("#percent").val();
    var produto = $("#produto").val();

    if( grupo == '' ||
        fazenda == '' ||
        percent == '' ||
        produto == '')
    {
        swal({
            title: 'Preencha o(s) campo(s) corretamente!',
            icon: 'warning'
        });
    }else{
        swal({
            text: 'Aguarde...',
            closeModal: false,
            buttons: false,
            closeOnClickOutside: false
        });

        // Formulario
        var form = $("#formRota").serialize();

        // Ajax
        $.ajax({
            url: '/relatorio-rota',
            type: 'POST',
            data: form,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data){
                // Limpa a tabela com a nova requisição
                $("#tableRota>tbody>tr").remove();

                // optRota
                var optRota = "<select class='form-control'><option>Rota da moto</option><option>Rota" +
                    " do Trator</option><option>Rota" +
                    " do Vagãozinho</option></select>";


                // Coloca as informações na tabela
                for(i=0; i < data.length; i++){
                    row = "<tr id='line-"+i+"'><td>"+optRota+"</td><td>"+data[i].local+"</td><td>"+data[i].media+"</td><td>"+data[i].porcentagem+"</td><td>"+data[i].perc_media+"</td><td>"+data[i].mediaxanimal+"</td><td>"+data[i].produto+"</td><td>"+data[i].realizado+"</td><td>"+data[i].deposito+"</td><td><i onClick='removeLine("+i+")' style='color:red;font-size:16px;cursor:pointer' class='fas fa-trash-alt'></i></td></tr>";

                    // Coloca os resultados na tabela
                    $("#tableRota>tbody").append(row);
                }





                // Remove o alerta
                swal.close();
            }
        });
    }
}


function removeLine(line){
    $("#line-"+line).remove();
}

function getDashboardLoc(local){
    if(local != ''){
        swal({
            text: 'Aguarde...',
            closeModal: false,
            buttons: false,
            closeOnClickOutside: false
        });

        // Ajax
        $.ajax({
            url: '/dashboard-local',
            type: 'POST',
            data: {'local': local},
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data){

                // Seta o total de gado e lotes no cabeçalho
                $("#cGado").html(data.gado);
                $("#cLote").html(data.lotes);

                var point = [];
                for(i=0 ; i < data['produtos'].length; i++){
                    point.push({y: data['produtos'][i].quantidade, label: data['produtos'][i].produtos.produto});
                }

                var chart = new CanvasJS.Chart("chartContainer", {
                    animationEnabled: true,
                    theme: "light2",
                    data: [{
                        type: "column",
                        showInLegend: true,
                        legendMarkerColor: "grey",
                        legendText: "Estoque em KG",
                        dataPoints: point
                    }]
                });
                chart.render();

                // Remove o alert
                swal.close();
            }
        });
    }

}

// Mascara de peso
$(document).ready(function() {
    $('.peso').maskMoney({thousands:'', decimal:'.', allowZero:true});
});
