<script type="text/javascript">
    window.onload = function () {

        var chart = new CanvasJS.Chart("chartContainer", {
            animationEnabled: true,
            theme: "light2", // "light1", "light2", "dark1", "dark2"
            data: [{
                type: "column",
                showInLegend: true,
                legendMarkerColor: "grey",
                legendText: "Estoque em KG",
                dataPoints: [
                        @foreach($produtos AS $key => $est)
                    { y: {{ $est->estoque->quantidade }}, label: "{{ $est->produto }}" },
                    @endforeach

                ]
            }]
        });
        chart.render();

    }
</script>

<div class="row">
    <div class="col-md-12">
        <div class="card ">
            <div class="card-header ">
                <h5 class="card-title">Estoque</h5>
            </div>
            <div class="card-body ">
                <div id="chartContainer" style="height: 300px; width: 100%;"></div>
            </div>
        </div>
    </div>
</div>

<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>