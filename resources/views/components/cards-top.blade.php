<div class="row">
    <div class="col-md-12">
        <div class="col-md-4 float-right">
            <select onchange="getDashboardLoc(this.value)" name="" id="" class="form-control float-right">
                <option value="">Selecione a Localidade</option>
                @foreach($localidades AS $loc)
                    <option value="{{ $loc->local }}">{{ $loc->local }}</option>
                @endforeach
            </select>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <hr>
    </div>
</div>

<div class="row">
    <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-stats">
            <div class="card-body ">
                <div class="row">
                    <div class="col-5 col-md-4">
                        <div class="icon-big text-center icon-warning">
                            <i style="color: #8e7e2d" class="far fa-cow"></i>
                        </div>
                    </div>
                    <div class="col-7 col-md-8">
                        <div class="numbers">
                            <p class="card-category">Gado</p>
                            <p id="cGado" class="card-title">{{ $gado }}
                            <p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-stats">
            <div class="card-body ">
                <div class="row">
                    <div class="col-5 col-md-4">
                        <div class="icon-big text-center icon-warning">
                            <i style="color: #5f8e2d" class="far fa-th"></i>
                        </div>
                    </div>
                    <div class="col-7 col-md-8">
                        <div class="numbers">
                            <p class="card-category">Lotes</p>
                            <p id="cLote" class="card-title">{{ $lotes }}
                            <p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-stats">
            <div class="card-body ">
                <div class="row">
                    <div class="col-5 col-md-4">
                        <div class="icon-big text-center icon-warning">
                            <i style="color: #8e2d2d" class="fal fa-map-marked-alt"></i>
                        </div>
                    </div>
                    <div class="col-7 col-md-8">
                        <div class="numbers">
                            <p class="card-category">Locais</p>
                            <p class="card-title">{{ $locais }}
                            <p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-stats">
            <div class="card-body ">
                <div class="row">
                    <div class="col-5 col-md-4">
                        <div class="icon-big text-center icon-warning">
                            <i style="color: #2d538e" class="far fa-truck-container"></i>
                        </div>
                    </div>
                    <div class="col-7 col-md-8">
                        <div class="numbers">
                            <p class="card-category">Fornecedores</p>
                            <p class="card-title">{{ $fornecedores }}
                            <p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>