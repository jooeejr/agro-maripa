<div class="sidebar" data-color="white" data-active-color="danger">
    <!--
      Tip 1: You can change the color of the sidebar using: data-color="blue | green | orange | red | yellow"
  -->
    <div class="logo">
        <a href="#" class="simple-text logo-mini">
            <div class="logo-image-small">
                <img src="{{ asset('dashboard/assets/img/logo-small.png') }}">
            </div>
        </a>
        <a href="#" class="simple-text logo-normal">
            {{ Auth::user()->name }}
        </a>
    </div>
    <div class="sidebar-wrapper">
        <ul class="nav">
            <li {{ ($current == 'dashboard') ? "class=active" : "" }}>
                <a href="/home">
                    <i class="fas fa-chart-line"></i>
                    <p>Dashboard</p>
                </a>
            </li>
            <li {{ ($current == 'lancamentos') ? "class=active" : "" }}>
                <a href="/lancamentos">
                {{--<a href="#">--}}
                    <i class="fas fa-file-signature"></i>
                    <p>Lançamentos</p>
                </a>
            </li>
            <li {{ ($current == 'fornecedores') ? "class=active" : "" }}>
                <a href="/fornecedores">
                    <i class="fas fa-industry"></i>
                    <p>Fornecedores</p>
                </a>
            </li>
            <li {{ ($current == 'produtos') ? "class=active" : "" }}>
                <a href="/produtos">
                    <i class="fas fa-store"></i>
                    <p>Produtos</p>
                </a>
            </li>
            <li {{ ($current == 'racoes') ? "class=active" : "" }}>
                <a href="/racoes">
                    <i class="fas fa-wheat"></i>
                    <p>Rações</p>
                </a>
            </li>
            <li {{ ($current == 'lotes') ? "class=active" : "" }}>
                <a href="/lotes">
                    <i class="fas fa-cubes"></i>
                    <p>Lotes</p>
                </a>
            </li>
            <li {{ ($current == 'consumo') ? "class=active" : "" }}>
                <a href="/consumo">
                    <i class="fas fa-tachometer-average"></i>
                    <p>Consumo Animal</p>
                </a>
            </li>

            <li {{ ($current == 'peso') ? "class=active" : "" }}>
                <a href="peso">
                    <i class="fas fa-weight"></i>
                    <p>Ganho de Peso</p>
                </a>
            </li>

            <li {{ ($current == 'rota') ? "class=active" : "" }}>
                <a href="rota">
                    <i class="fas fa-road"></i>
                    <p>Rotas</p>
                </a>
            </li>

            <li>
                <a onClick="return logout()" href="#">
                    <i style="color: red" class="fas fa-power-off"></i>
                    <p style="color: red">Sair</p>
                </a>
            </li>


        </ul>
    </div>
</div>