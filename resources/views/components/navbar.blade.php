<style>
    .list-notificacao {
        min-width: 400px;
        background: #ffffff;
    }

    .list-notificacao li {
        border-bottom: 1px #d8d8d8 solid;
        text-align: justify;
        padding: 5px 10px 5px 10px;
        cursor: pointer;
        font-size: 12px;
    }

    .list-notificacao li:hover {
        background: #f1eeee;
    }

    .list-notificacao li:hover .exclusaoNotificacao {
        display: block;
    }

    .list-notificacao li p {
        color: black;
        width: 305px;
    }

    .list-notificacao li .exclusaoNotificacao {
        width: 25px;
        min-height: 40px;
        position: absolute;
        right: 0;
        display: none;
    }

    .list-notificacao .media img {
        width: 40px;
        height: 40px;
        float: left;
        margin-right: 10px;
    }

    .badgeAlert {
        display: inline-block;
        min-width: 10px;
        padding: 3px 7px;
        font-size: 12px;
        font-weight: 700;
        color: #fff;
        line-height: 1;
        vertical-align: baseline;
        white-space: nowrap;
        text-align: center;
        background-color: #d9534f;
        border-radius: 10px;
        position: absolute;
        margin-top: -10px;
        margin-left: -10px
    }
</style>

<nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
    <div class="container-fluid">
        <div class="navbar-wrapper">
            <div class="navbar-toggle">
                <button type="button" class="navbar-toggler">
                    <span class="navbar-toggler-bar bar1"></span>
                    <span class="navbar-toggler-bar bar2"></span>
                    <span class="navbar-toggler-bar bar3"></span>
                </button>
            </div>
            <a class="navbar-brand" href="#pablo">Painel Administrativo</a>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                           aria-expanded="false">
                            <i style="font-size: 20px;" class="fas fa-bell"></i>
                            <span class='badgeAlert'>{{ count($notificacao) }}</span>
                           </a>
                        <ul class="list-notificacao dropdown-menu">
                            @foreach($notificacao AS $not)
                            <li>
                                <div class="media">
                                    <div class="media-left">
                                        <a href="#"></a>
                                    </div>
                                    <div class="media-body">
                                        <h4 class="media-heading">{{ $not->localidade.' - '.$not->racoes->racao }}</h4>
                                    </div>
                                </div>
                            </li>
                            @endforeach
                            {{--<li id='item_notification_2'>--}}
                                {{--<div class="media">--}}
                                    {{--<div class="media-left">--}}
                                        {{--<a href="#"></a>--}}
                                    {{--</div>--}}
                                    {{--<div class="media-body">--}}
                                        {{--<h4 class="media-heading">asd</h4>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</li>--}}
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation"
                aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-bar navbar-kebab"></span>
            <span class="navbar-toggler-bar navbar-kebab"></span>
            <span class="navbar-toggler-bar navbar-kebab"></span>
        </button>
        {{--<div class="collapse navbar-collapse justify-content-end" id="navigation">--}}
        {{--<form>--}}
        {{--<div class="input-group no-border">--}}
        {{--<input type="text" value="" class="form-control" placeholder="Search...">--}}
        {{--<div class="input-group-append">--}}
        {{--<div class="input-group-text">--}}
        {{--<i class="nc-icon nc-zoom-split"></i>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--</form>--}}
        {{--<ul class="navbar-nav">--}}
        {{--<li class="nav-item">--}}
        {{--<a class="nav-link btn-magnify" href="#pablo">--}}
        {{--<i class="nc-icon nc-layout-11"></i>--}}
        {{--<p>--}}
        {{--<span class="d-lg-none d-md-block">Stats</span>--}}
        {{--</p>--}}
        {{--</a>--}}
        {{--</li>--}}
        {{--<li class="nav-item btn-rotate dropdown">--}}
        {{--<a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}
        {{--<i class="nc-icon nc-bell-55"></i>--}}
        {{--<p>--}}
        {{--<span class="d-lg-none d-md-block">Some Actions</span>--}}
        {{--</p>--}}
        {{--</a>--}}
        {{--<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">--}}
        {{--<a class="dropdown-item" href="#">Action</a>--}}
        {{--<a class="dropdown-item" href="#">Another action</a>--}}
        {{--<a class="dropdown-item" href="#">Something else here</a>--}}
        {{--</div>--}}
        {{--</li>--}}
        {{--<li class="nav-item">--}}
        {{--<a class="nav-link btn-rotate" href="#pablo">--}}
        {{--<i class="nc-icon nc-settings-gear-65"></i>--}}
        {{--<p>--}}
        {{--<span class="d-lg-none d-md-block">Account</span>--}}
        {{--</p>--}}
        {{--</a>--}}
        {{--</li>--}}
        {{--</ul>--}}
        {{--</div>--}}
    </div>
</nav>