{{--Layout Base--}}
@extends('layouts.layout', ['current' => 'consumo'])



@section('conteudo')
    <style>
        .space{
            margin-bottom: 2%;
        }

        select{
            height: 32px!important;
        }
    </style>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="col-md-12">
                        <h5 class="card-title">Relatório de Consumo</h5>
                    </div>
                </div>
                <div class="card-body ">
                    <form id="formConsumo" method="post" action="/consumo">
                        @csrf
                        <div class="row">
                            <div class="col-md-4 float-left space">
                                <select class="form-control" name="localidade"
                                        id="localidade">
                                    <option value="">Selecione a localidade</option>
                                    @foreach($localidades AS $loc)
                                        <option value="{{ $loc->localidade }}">{{ $loc->localidade }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-4 float-left space">
                                De:
                                <input id="de" required class="form-control" name="de" type="date">
                            </div>

                            <div class="col-md-4 float-left space">
                                Até
                                <input id="ate" required class="form-control" name="ate" type="date">
                            </div>
                        </div>

                        <div class="col-md-12 float-left space">
                            <button onClick="consumoAnimal()" type="button" class="btn btn-success float-right">
                                Buscar
                            </button>
                        </div>
                    </form>
                </div>
            </div>

            <div class="card">
                <div class="card-footer">
                    <div class="col-md-12">
                        <h5 class="card-title">Dados do Consumo</h5>
                        <hr style="border: 1px solid #ddd">
                    </div>

                    <div class="col-md-12">
                        <h5 style="font-size: 16px"> Consumo Previsto:
                            <b id="consumoPrevisto" style="color: #ef8157">0.00 KG</b>
                        </h5>

                        <h5 style="font-size: 16px"> Consumo Real:
                            <b id="consumoReal" style="color: #ef8157">0.00 KG</b>
                        </h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection