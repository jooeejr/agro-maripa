{{--Layout Base--}}
@extends('layouts.layout', ['current' => 'dashboard'])

@section('conteudo')
    {{--Card do Topo--}}
    @component('components.cards-top', ['localidades' => $localidades, 'gado' => $gado, 'lotes' => $lotes,
    'fornecedores' => $fornecedores, 'locais' => $locais])
    @endcomponent

    {{--Card Central--}}
    @component('components.cards-center', ['produtos' => $produtos])
    @endcomponent

    {{--Card do fim--}}
    @component('components.cards-final')
    @endcomponent
@endsection