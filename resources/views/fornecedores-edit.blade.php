{{--Layout Base--}}
@extends('layouts.layout', ['current' => 'fornecedores'])



@section('conteudo')
    <style>
        .space{
            margin-bottom: 2%;
        }

        select{
            height: 32px!important;
        }
    </style>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="col-md-12">
                        <h5 class="card-title">Edição de Fornecedor</h5>
                    </div>
                </div>
                <div class="card-body ">
                    <form id="formEditFornecedor" method="post" action="/fornecedores/{{ $fornecedor->id }}">
                        @csrf
                        @method('patch')
                        <div class="col-md-6 float-left space">
                            <input value="{{ $fornecedor->fornecedor }}" id="fornecedor" placeholder="Fornecedor" class="form-control" name="fornecedor" type="text">
                        </div>

                        <div class="col-md-12 float-left">
                            <hr>
                        </div>

                        <div class="col-md-12 float-right space">
                            <button onClick="attFornecedor()" type="button" class="btn btn-success float-right">
                                Atualizar
                            </button>

                            <button onClick="window.location.href='/fornecedores'" type="button" class="btn btn-danger float-right">
                                Voltar
                            </button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
@endsection