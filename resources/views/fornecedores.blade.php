{{--Layout Base--}}
@extends('layouts.layout', ['current' => 'fornecedores'])



@section('conteudo')
    <style>
        .space{
            margin-bottom: 2%;
        }

        select{
            height: 32px!important;
        }
    </style>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="col-md-12">
                        <h5 class="card-title">Cadastro de Fornecedores</h5>
                    </div>
                </div>
                <div class="card-body ">
                    <form id="formFornecedor" method="post" action="/fornecedores">
                        @csrf
                        <div class="col-md-6 float-left space">
                            <input id="fornecedor" placeholder="Fornecedor" class="form-control" name="fornecedor" type="text">
                        </div>

                        <div class="col-md-12 float-left space">
                            <button onClick="cadFornecedor()" type="button" class="btn btn-success float-right">
                                Cadastrar
                            </button>
                        </div>
                    </form>
                </div>
            </div>

            <div class="card">
                <div class="card-footer">
                    <table id="example" style="text-align: center;" class="table table-hover">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Fornecedor</th>
                            <th>Ação</th>
                        </tr>
                        </thead>

                        <tbody>
                        @foreach($fornecedores As $fornecedor)
                            <tr>
                                <td>{{ $fornecedor->id }}</td>
                                <td>{{ $fornecedor->fornecedor }}</td>
                                <td>
                                    <button onClick="window.location.href='/fornecedores/{{ $fornecedor->id }}'" class="btn btn-warning">
                                        <i class="fas fa-edit"></i>
                                    </button>

                                    &nbsp;&nbsp;&nbsp;
                                    <button onClick="removeFornecedor({{ $fornecedor->id }})" class="btn btn-danger">
                                        <i class="fas fa-remove"></i>
                                    </button>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(function() {
            $('#example')
                .addClass( 'nowrap' )
                .DataTable({
                    responsive: true,
                    "language": {
                        "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Portuguese.json"
                    }
                });
        });
    </script>
@endsection