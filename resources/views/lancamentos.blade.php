{{--Layout Base--}}
@extends('layouts.layout', ['current' => 'lancamentos'])

@section('conteudo')
    <style>
        .space{
            margin-bottom: 2%;
        }

        select{
            height: 32px!important;
        }

        i{
            font-size: 15px;
        }
    </style>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-12">
                            <h5 class="card-title">Lançamentos</h5>
                        </div>
                    </div>
                </div>
                <div class="card-body ">
                    <form id="formLancamento" method="post" action="/lancamentos">
                        @csrf
                        <div class="row">
                            <div class="col-md-2 float-left space">
                                <input name="created_at" value="{{ date('Y-m-d') }}" id="data" placeholder="Data"
                                       class="form-control"
                                       type="date">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4 float-left space">
                                <select onchange="buscaLote(this.value)" class="form-control" name="localidade" id="localidade">
                                    <option value="">Selecione a localidade...</option>
                                    @foreach($localidades AS $loc)
                                        <option value="{{ $loc->local }}">{{ $loc->local }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-md-4 float-left space">
                                <select class="form-control" name="fazenda"
                                        id="fazenda">
                                    <option value="">Selecione a fazenda...</option>
                                </select>
                            </div>


                            <div class="col-md-4 float-left space">
                                <select onchange="calculaMediaPeso(this.value)" class="form-control" name="grupo"
                                        id="grupo">
                                    <option value="">Selecione o Grupo...</option>
                                </select>
                            </div>


                            <div class="col-md-2 float-left space">
                                <input placeholder="Peso Médio" id="peso_medio" name="peso_medio" class="form-control peso" type="text">
                            </div>

                            <div class="col-md-2 float-left space">
                                <input placeholder="Total de animais" id="total_animais" name="total_animais" class="form-control" type="text">
                            </div>

                            <div class="col-md-2 float-left space">
                                <input id="lembrete" name="lembrete" class="form-control" type="date">
                            </div>

                            <div style="font-size: 18px;" class="col-md-2 float-left space">
                                &rarr; Data do Lembrete
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-4 float-left space">
                                <select class="form-control" name="racao" id="racao">
                                    <option value="">Selecione a Ração</option>
                                    @foreach($racoes AS $racao)
                                        <option value="{{ $racao->id }}">{{ $racao->racao }}</option>
                                    @endforeach
                                </select>
                            </div>


                            <div class="col-md-4 float-left space">
                                <select class="form-control" name="leitura" id="leitura">
                                    <option value="">Leitura</option>
                                    <option value="0">Nada</option>
                                    <option value="1">Pouco</option>
                                    <option value="2">Cheio</option>
                                </select>
                            </div>

                            <div class="col-md-2 float-left space">
                                <input placeholder="Previsto" id="previsto" name="previsto" class="form-control peso"
                                       type="text">
                            </div>

                            <div class="col-md-2 float-left space">
                                <input placeholder="Realizado" id="realizado" name="realizado" class="form-control
                                peso" type="text">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 float-left space">
                                <button onClick="cadLancamento()" type="button" class="btn btn-success float-right">
                                    Cadastrar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <div class="card">
                <div class="card-footer">
                    <table id="lotes" style="text-align: center;" class="table table-hover">
                        <thead>
                        <tr>
                            <th>Localidade</th>
                            <th>Peso medio</th>
                            <th>Animais</th>
                            <th>Ração</th>
                            <th>Leitura</th>
                            <th>Previsto</th>
                            <th>Realizado</th>
                            <th>Data</th>
                        </tr>
                        </thead>

                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(function() {
            $('#lotes')
                .addClass( 'nowrap' )
                .DataTable({
                    processing: true,
                    serverSide: true,
                        ajax: '/dados-lancamentos',
                    responsive: true,
                    columns: [
                        {data: 'localidade', name: 'localidade'},
                        {data: 'peso_medio', name: 'peso_medio'},
                        {data: 'total_animais', name: 'total_animais'},
                        {data: 'racao_id', name: 'racao_id.racao'},
                        {data: 'leitura', name: 'leitura'},
                        {data: 'previsto', name: 'previsto'},
                        {data: 'realizado', name: 'realizado'},
                        {data: 'created_at', name: 'created_at'}
                    ],
                    "language": {
                        "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Portuguese.json"
                    }
                });
        });
    </script>
@endsection