<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('dashboard/assets/img/apple-icon.png') }}">
    <link rel="icon" type="image/png" href="{{ asset('dashboard/assets/img/favicon.png') }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>
        Controle de Consumo
    </title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />

    <!--     Fonts and icons     -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/fontawesome/css/all.css') }}">


    <!-- CSS Files -->
    <link href="{{ asset('dashboard/assets/css/bootstrap.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('dashboard/assets/css/paper-dashboard.css?v=2.0.0') }}" rel="stylesheet" />
    <link href="{{ asset('dashboard/assets/demo/demo.css') }}" rel="stylesheet" />
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.1/css/responsive.dataTables.min.css">

    {{--JS--}}
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script type="text/javascript" src="{{ asset('js/main.js') }}"></script>

</head>

<body>
<div class="wrapper ">

    {{--Menu Lateral--}}
    @component('components.menu-lateral', ['current' => $current])
    @endcomponent


    <div class="main-panel">

        <!-- Navbar -->
    @component('components.navbar')
    @endcomponent
    <!-- End Navbar -->

        <div class="content">
            {{--Conteudo--}}
            @hasSection('conteudo')
                @yield('conteudo')
            @endif
        </div>
    </div>
</div>
<!--   Core JS Files   -->
<script src="{{ asset('dashboard/assets/js/core/jquery.min.js') }}"></script>
<script src="{{ asset('dashboard/assets/js/core/popper.min.js') }}"></script>
<script src="{{ asset('dashboard/assets/js/core/bootstrap.min.js') }}"></script>
<script src="{{ asset('dashboard/assets/js/plugins/perfect-scrollbar.jquery.min.js') }}"></script>
<!-- Chart JS -->
<script src="{{ asset('dashboard/assets/js/plugins/chartjs.min.js') }}"></script>
<!--  Notifications Plugin    -->
<script src="{{ asset('dashboard/assets/js/plugins/bootstrap-notify.js') }} "></script>
<!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
<script src="{{ asset('dashboard/assets/js/paper-dashboard.min.js?v=2.0.0') }}" type="text/javascript"></script>
<!-- Paper Dashboard DEMO methods, don't include it in your project! -->
<script src="{{ asset('dashboard/assets/demo/demo.js') }}"></script>
{{--mask money--}}
<script src="{{ asset('js/mask/src/jquery.maskMoney.js') }}" type="text/javascript"></script>
{{--Datatable--}}
<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.min.js"></script>
<script>
    $(document).ready(function() {
        // Javascript method's body can be found in assets/assets-for-demo/js/demo.js
        demo.initChartsPages();
    });
</script>
</body>

</html>