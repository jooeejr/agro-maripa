{{--Layout Base--}}
@extends('layouts.layout', ['current' => 'lotes'])



@section('conteudo')
    <style>
        .space{
            margin-bottom: 2%;
        }

        select{
            height: 32px!important;
        }
    </style>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="col-md-12">
                        <h5 class="card-title">Edição de Lote</h5>
                    </div>
                </div>
                <div class="card-body ">
                    <form id="formEditLote" method="post" action="/lotes/{{ $lote->id }}">
                        @csrf
                        @method('patch')
                        <div class="col-md-6 float-left space">
                            <input value="{{ $lote->lote }}" id="lote" placeholder="Lotes" class="form-control" name="lote" type="text">
                        </div>

                        <div class="col-md-12 float-left space">
                            <button onClick="attLote()" type="button" class="btn btn-success float-right">
                                Atualizar
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection