{{--Layout Base--}}
@extends('layouts.layout', ['current' => 'lotes'])



@section('conteudo')
    <style>
        .space{
            margin-bottom: 2%;
        }

        select{
            height: 32px!important;
        }
    </style>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="col-md-12">
                        <h5 class="card-title">Cadastro de Lotes</h5>
                    </div>
                </div>
                <div class="card-body ">
                    <form enctype="multipart/form-data" id="formLote" method="post" action="/lotes">
                        <input type="hidden" name="send" value="ok">
                        @csrf
                        <div class="col-md-6 float-left space">
                            <input required id="lote" placeholder="Lotes" class="form-control" name="lote" type="file">
                        </div>

                        <div class="col-md-12 float-left space">
                            <button onClick="cadLote()" type="button" class="btn btn-success float-right">
                                Cadastrar
                            </button>
                        </div>
                    </form>
                </div>
            </div>

            <div class="card">
                <div class="card-footer">
                    <table id="lotes" style="text-align: center;" class="table table-hover display responsive no-wrap">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Apelido</th>
                            <th>Sexo</th>
                            <th>Data Nasc.</th>
                            <th>Data Peso</th>
                            <th>Peso</th>
                            <th>Local</th>
                            <th>Lote Reprodutivo</th>
                            <th>Grupo</th>
                        </tr>
                        </thead>

                        {{--<tbody>--}}
                        {{--@foreach($lotes As $lote)--}}
                            {{--<tr>--}}
                                {{--<td>{{ $lote->id }}</td>--}}
                                {{--<td>{{ $lote->apelido }}</td>--}}
                                {{--<td>{{ $lote->peso }}</td>--}}
                                {{--<td>{{ $lote->local }}</td>--}}
                                {{--<td>{{ $lote->lote_reprodutivo }}</td>--}}
                            {{--</tr>--}}
                        {{--@endforeach--}}
                        {{--</tbody>--}}
                    </table>
                </div>
            </div>
        </div>
    </div>


    <script type="text/javascript">
        $(function() {
            $('#lotes')
                .addClass( 'nowrap' )
                .DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: '/dados-lotes',
                    responsive: true,
                    columns: [
                        {data: 'id'},
                        {data: 'apelido'},
                        {data: 'sexo'},
                        {data: 'data_nascimento'},
                        {data: 'data_peso'},
                        {data: 'peso'},
                        {data: 'local'},
                        {data: 'lote_reprodutivo'},
                        {data: 'grupo'}
                    ],
                    "language": {
                        "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Portuguese.json"
                    }
            });
        });
    </script>
@endsection