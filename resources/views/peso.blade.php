{{--Layout Base--}}
@extends('layouts.layout', ['current' => 'peso'])



@section('conteudo')
    <style>
        .space{
            margin-bottom: 2%;
        }

        select{
            height: 32px!important;
        }

        #titleImage{
            display: none;
            padding: 15px;
        }

        #titleRel{
            display: none;
        }


        @media print {

            #btnImpressao{
                display: none;
            }

            #titleImage{
                display: block;
            }
            #titleText{
                display: none;
            }

            #titleRel{
                display: block;
            }
            @page { margin: 0; }
            body { margin: 1.6cm; }

            .sidebar{
                display: none;
            }
            
            #cardFilter{
                display: none;
            }

            #cardResult{
                display: block;
                position: fixed;
                top: 0;
                left: 0;
                width: 100%;
            }
        }
    </style>

    <div class="row">
        <div class="col-md-12">
            <div id="cardFilter" class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-12">
                            <h5 class="card-title">Relatório de Ganho de Peso</h5>
                        </div>
                    </div>
                </div>
                <div class="card-body ">
                    <form id="formPeso" method="post">
                        @csrf
                        <div class="row">
                            <div class="col-md-4 float-left space">
                                <select class="form-control" name="racao"
                                        id="racao">
                                    <option value="">Selecione a Ração</option>
                                    @foreach($racoes AS $rac)
                                        <option value="{{ $rac->racao }}">{{ $rac->racao }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-md-4 float-left space">
                                <select class="form-control" name="localidade"
                                        id="localidade">
                                    <option value="">Selecione a Localidade</option>
                                    @foreach($localidade AS $loc)
                                        <option value="{{ $loc->local }}">{{ $loc->local }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-4 float-left space">
                                Inicio Tratamento:
                                <input id="de" required class="form-control" name="de" type="date">
                            </div>

                            <div class="col-md-4 float-left space">
                                Fim Tratamento
                                <input id="ate" required class="form-control" name="ate" type="date">
                            </div>

                            <div class="col-md-2">
                                Ganho Diário
                                <input id="ganho" class="form-control peso" type="text">
                            </div>
                        </div>

                        <div class="col-md-12 float-left space">
                            <button onClick="ganhoDePeso()" type="button" class="btn btn-success float-right">
                                Gerar Relatório
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div id="cardResult" class="card">
        <div class="card-footer">
            <div class="col-md-12">
                <h5 id="titleText" align="center" class="card-title">
                    Resultado do relatório
                </h5>
                <h5 id="titleImage" align="center" class="card-title">
                    <img src="{{ asset('img/logo.png') }}" alt="">
                </h5>
                <hr style="border: 1px solid #ddd">
            </div>

            <div class="col-md-12">
                <h5 id="titleRel" class="card-title">Relatório: Ganho de Peso</h5> <br>
                <h5 style="font-size: 16px"> Localidade:
                    <b id="localidadeRel" style="color: #ef8157">-</b>
                </h5>

                <h5 style="font-size: 16px"> Ração Utilizada:
                    <b id="racaoUtilizada" style="color: #ef8157">-</b>
                </h5>

                <h5 style="font-size: 16px"> Inicio do Tratamento:
                    <b id="inicio" style="color: #ef8157">-</b>
                </h5>

                <h5 style="font-size: 16px"> Fim do Tratamento:
                    <b id="fim" style="color: #ef8157">-</b>
                </h5>

                <h5 style="font-size: 16px"> Ganho Total (KG):
                    <b id="pesoTotal" style="color: #ef8157">-</b>
                </h5>
            </div>

            <div class="col-md-4 float-right">
                <button id="btnImpressao" onclick="window.print()" type="button" class="btn btn-primary float-right">
                    <i class="fas fa-print"></i>
                    Imprimir
                </button>
            </div>
        </div>
    </div>
@endsection