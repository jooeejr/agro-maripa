{{--Layout Base--}}
@extends('layouts.layout', ['current' => 'produtos'])



@section('conteudo')
    <style>
        .space{
            margin-bottom: 2%;
        }

        select{
            height: 32px!important;
        }
    </style>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="col-md-12">
                        <h5 class="card-title">Edição de Produto</h5>
                    </div>
                </div>
                <div class="card-body ">
                    <form id="formEditProduto" method="post" action="/produtos/{{ $produto->id }}">
                        @csrf
                        @method('patch')
                        <div class="col-md-12 float-left space">
                            <input value="{{ $produto->produto }}" id="produto" placeholder="Produto" class="form-control" name="produto" type="text">
                        </div>

                        <div class="col-md-12 float-left space">
                            <button onClick="attProduto()" type="button" class="btn btn-success float-right">
                                Atualizar
                            </button>

                            <button onClick="window.location.href='/produtos'" type="button" class="btn btn-danger float-right">
                                Voltar
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection