{{--Layout Base--}}
@extends('layouts.layout', ['current' => 'produtos'])


@section('conteudo')
    <style>
        .space{
            margin-bottom: 2%;
        }

        select{
            height: 32px!important;
        }

        i{
            font-size: 15px;
        }
    </style>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="col-md-12">
                        <h5 class="card-title">Lançamento: <b>{{ $produto->produto }}</b></h5>
                    </div>
                </div>
                <div class="card-body">
                    <form id="formProduto" method="post" action="/lancamento-produto/{{ $produto->id }}">
                        @csrf

                        <div class="row">
                            <div class="col-md-4 float-left space">
                                <select id="fornecedor" name="fornecedor" class="form-control {{ ($errors->has('fornecedor')) ? 'is-invalid' : '' }}">
                                    @foreach($fornecedores AS $fornecedor)
                                        <option {{ ($fornecedor->id == old('fornecedor')) ? 'selected' : '' }} value="{{ $fornecedor->id }}">{{ $fornecedor->fornecedor }}</option>
                                    @endforeach
                                </select>

                                @if($errors->has('fornecedor'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('fornecedor') }}
                                    </div>
                                @endif

                            </div>

                            <div class="col-md-2 float-left space">
                                <input value="{{ old('preco') }}" id="preco" placeholder="Preço" class="form-control {{ ($errors->has('preco')) ? 'is-invalid' : '' }} peso" name="preco" type="text">

                                @if($errors->has('preco'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('preco') }}
                                    </div>
                                @endif
                            </div>

                            <div class="col-md-2 float-left space">
                                <input value="{{ old('qtd') }}" id="quantidade" placeholder="Qtd (KG)" class="form-control {{ ($errors->has('quantidade')) ? 'is-invalid' : '' }} peso" name="qtd" type="text">

                                @if($errors->has('qtd'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('qtd') }}
                                    </div>
                                @endif
                            </div>

                            <div class="col-md-4 float-left space">
                                <input value="{{ old('nf') }}" id="nf" placeholder="NF" class="form-control {{ ($errors->has('nf')) ? 'is-invalid' : '' }}" name="nf" type="text">

                                @if($errors->has('nf'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('nf') }}
                                    </div>
                                @endif
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-4 float-left space">
                                <select id="localidade" name="localidade" class="form-control {{ ($errors->has
                            ('localidade')) ?
                            'is-invalid' : '' }}">
                                    <option value="">Selecione a Localidade...</option>
                                    @foreach($localidades AS $loc)
                                        <option {{ ($loc->local == old('localidade')) ? 'selected' : '' }} value="{{
                                    $loc->local }}">{{ $loc->local }}</option>
                                    @endforeach
                                </select>

                                @if($errors->has('localidade'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('localidade') }}
                                    </div>
                                @endif
                            </div>
                        </div>


                        <div class="col-md-12 float-left space">
                            <button onClick="cadProduto()" type="button" class="btn btn-success float-right">
                                Lançar
                            </button>

                            <button onClick="window.location.href='/produtos'" type="button" class="btn btn-danger float-right">
                                Voltar
                            </button>
                        </div>
                    </form>
                </div>
            </div>


            <div class="card">
                <div class="card-body">
                    <table id="example" class="table table-hover">
                        <thead>
                            <tr>
                                <th>Lançamento</th>
                                <th>Local</th>
                                <th>Qtd (KG)</th>
                                <th>Fornecedor</th>
                                <th>Preço</th>
                                <th>NF</th>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach($lancamentos AS $lc)
                                <tr>
                                    <td>{{ date('d/m/Y H:i', strtotime($lc->created_at)) }}</td>
                                    <td>{{ $lc->local }}</td>
                                    <td>{{ $lc->quantidade }}</td>
                                    <td>{{ $lc->fornecedor->fornecedor }}</td>
                                    <td>{{ $lc->preco }}</td>
                                    <td>{{ $lc->nf }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>


        </div>
    </div>

    <script type="text/javascript">
        $(function() {
            $('#example')
                .addClass( 'nowrap' )
                .DataTable({
                    responsive: true,
                    "language": {
                        "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Portuguese.json"
                    }
                });
        });
    </script>
@endsection