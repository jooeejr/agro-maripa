{{--Layout Base--}}
@extends('layouts.layout', ['current' => 'produtos'])

@section('conteudo')
    <style>
        .space{
            margin-bottom: 2%;
        }

        select{
            height: 32px!important;
        }

        i{
            font-size: 15px;
        }
    </style>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="col-md-12">
                        <h5 class="card-title">Cadastro de Produtos</h5>
                    </div>
                </div>
                <div class="card-body ">
                    <form id="formProduto" method="post" action="/produtos">
                        @csrf
                        <div class="col-md-12 float-left space">
                            <input id="produto" placeholder="Produto" class="form-control" name="produto" type="text">
                        </div>

                        <div class="col-md-12 float-left space">
                            <button onClick="cadProduto()" type="button" class="btn btn-success float-right">
                                Cadastrar
                            </button>
                        </div>
                    </form>
                </div>
            </div>

            <div class="card">
                <div class="card-footer">
                    <table id="example" style="text-align: center;" class="table table-hover">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Produto</th>
                            <th>Estoque</th>
                            <th>Ação</th>
                        </tr>
                        </thead>

                        <tbody>
                        @foreach($produtos As $prod)
                            <tr>
                                <td>{{ $prod->id }}</td>
                                <td>{{ $prod->produto }}</td>
                                <td>{{ $prod->estoque->quantidade }}</td>
                                <td>
                                    <button onClick="window.location.href='/lancamento-produto/{{ $prod->id }}'" class="btn btn-primary">
                                        <i class="fas fa-plus"></i>
                                    </button>

                                    <button onClick="window.location.href='/produtos/{{ $prod->id }}'" class="btn btn-warning">
                                        <i class="fas fa-edit"></i>
                                    </button>

                                    <button onClick="removeProduto({{ $prod->id }})" class="btn btn-danger">
                                        <i class="fas fa-remove"></i>
                                    </button>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(function() {
            $('#example')
                .addClass( 'nowrap' )
                .DataTable({
                    responsive: true,
                    "language": {
                        "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Portuguese.json"
                    }
                });
        });
    </script>
@endsection