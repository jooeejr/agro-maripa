{{--Layout Base--}}
@extends('layouts.layout', ['current' => 'racoes'])



@section('conteudo')
    <style>
        .space{
            margin-bottom: 2%;
        }

        select{
            height: 32px!important;
        }
    </style>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="col-md-12">
                        <h5 class="card-title">Edição de Ração</h5>
                    </div>
                </div>
                <div class="card-body ">
                    <form id="formEditRacao" method="post" action="/racoes/{{ $racao->id }}">
                        @csrf
                        @method('patch')
                        <div class="col-md-12 float-left space">
                            <input value="{{ $racao->racao }}" id="produto" placeholder="Produto" class="form-control" name="racao" type="text">
                        </div>

                        <div class="col-md-12 float-left space">
                            <button onClick="attRacao()" type="button" class="btn btn-success float-right">
                                Atualizar
                            </button>

                            <button onClick="window.location.href='/racoes'" type="button" class="btn btn-danger float-right">
                                Voltar
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection