{{--Layout Base--}}
@extends('layouts.layout', ['current' => 'racoes'])



@section('conteudo')
    <style>
        .space{
            margin-bottom: 2%;
        }

        select{
            height: 32px!important;
        }
    </style>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-12">
                            <h5 class="card-title">Montagem de Ração: <b style="color:#ef8157">{{ $racao->racao.' - '.$racao->quantidade.' KG' }}</b></h5>
                        </div>
                    </div>
                </div>
                <div class="card-body ">
                    <form id="formMontagem" method="post" action="/montar-racao/{{ $racao->id }}">
                        @csrf
                        <div class="row">
                            <div class="col-md-2">
                                <button id="addProduct" type="button" class="btn btn-primary">
                                    <i class="fas fa-cart-plus"></i> &nbsp;
                                    Adicionar Produto
                                </button>
                            </div>
                        </div>

                        <div id="firstProduct" class="row products">
                            <div class="col-md-4 float-left space">
                                <select required class="form-control inpValidate" name="produto[]">
                                    <option value="">Selecione um Produto</option>
                                    @foreach($produtos AS $prod)
                                        <option value="{{ $prod->id }}">{{ $prod->produto }}</option>
                                    @endforeach
                                </select>
                            </div>


                            <div class="col-md-2 float-left space">
                                <input required placeholder="Qtd (KG)" class="form-control peso inpValidate" name="qtd_produto[]" type="text">
                            </div>
                        </div>


                        <div class="col-md-12 float-left space">
                            <button type="button" onclick="cadMontagem()" class="btn btn-success float-right">
                                Cadastrar
                            </button>

                            <button onClick="window.location.href='/racoes'" type="button" class="btn btn-danger float-right">
                                Voltar
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-12">
                            <h5 class="card-title">Produtos</h5>
                        </div>
                    </div>
                </div>
                <div class="card-body ">
                    <table id="myTable" class="table table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Produto</th>
                                <th>Quantidade</th>
                                <th>Ação</th>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach($produtosRe AS $prodR)
                                <tr>
                                    <td>{{ $prodR->id }}</td>
                                    <td>{{ $prodR->produto->produto }}</td>
                                    <td>{{ $prodR->quantidade }}</td>
                                    <td>
                                        <button onclick="removeProdutoRacao({{ $prodR->id }})" type="button" class="btn btn-sm btn-danger">
                                            <i class="fal fa-trash-alt"></i>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    {{--Scripts--}}

    <script type="text/javascript">
        // Clona a estrutura de produtos
        $(document).ready(function() {
            $('#addProduct').on('click', function() {
                var total = $(".products").length;
                var estrutura = "<i onClick='delDiv("+total+")' style='font-size: 20px; margin-top: 6px; color:#ef8157; cursor: pointer' class='fas fa-trash-alt'></i>";
                $("#firstProduct")
                    .clone()
                    .insertAfter(".products:last")
                    .attr({"id":"div-"+total})
                    .append(estrutura);

                $(document).ready(function() {
                    $('.peso').maskMoney({thousands:'', decimal:'.', allowZero:true});
                });
            })
        });

        // Remove uma div
        function delDiv(id)
        {
            $("#div-"+id).remove();
        }


        // Datatable
        $(document).ready( function () {
            $('#myTable').DataTable();
        } );

    </script>
@endsection