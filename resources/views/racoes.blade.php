{{--Layout Base--}}
@extends('layouts.layout', ['current' => 'racoes'])

@section('conteudo')
    <style>
        .space{
            margin-bottom: 2%;
        }

        select{
            height: 32px!important;
        }

        i{
            font-size: 15px;
        }
    </style>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="col-md-12">
                        <h5 class="card-title">Cadastro de Rações</h5>
                    </div>
                </div>
                <div class="card-body ">
                    <form id="formRacao" method="post" action="/racoes">
                        @csrf
                        <div class="col-md-6 float-left space">
                            <input id="racao" placeholder="Ração" class="form-control" name="racao" type="text">
                        </div>
                        <div class="col-md-2 float-left space">
                            <input id="qtd" placeholder="Qtd (KG)" class="form-control peso" name="qtd" type="text">
                        </div>


                        <div class="col-md-12 float-left space">
                            <button onClick="cadRacao()" type="button" class="btn btn-success float-right">
                                Cadastrar
                            </button>
                        </div>
                    </form>
                </div>
            </div>

            <div class="card">
                <div class="card-footer">
                    <table id="example" style="text-align: center;" class="table table-hover">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Ração</th>
                            <th>Qtd (Kg)</th>
                            <th>Ação</th>
                        </tr>
                        </thead>

                        <tbody>
                        @foreach($racao As $rac)
                            <tr>
                                <td>{{ $rac->id }}</td>
                                <td>{{ $rac->racao }}</td>
                                <td>{{ $rac->quantidade }}</td>
                                <td>
                                    <button onClick="window.location.href='/montar-racao/{{ $rac->id }}'" class="btn btn-primary">
                                        <i class="fas fa-compress-arrows-alt"></i>
                                    </button>

                                    <button onClick="window.location.href='/racoes/{{ $rac->id }}'" class="btn btn-warning">
                                        <i class="fas fa-edit"></i>
                                    </button>

                                    <button onClick="removeRacao({{ $rac->id }})" class="btn btn-danger">
                                        <i class="fas fa-remove"></i>
                                    </button>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(function() {
            $('#example')
                .addClass( 'nowrap' )
                .DataTable({
                    responsive: true,
                    "language": {
                        "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Portuguese.json"
                    }
                });
        });
    </script>
@endsection