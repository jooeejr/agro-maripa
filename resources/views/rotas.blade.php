{{--Layout Base--}}
@extends('layouts.layout', ['current' => 'rota'])



@section('conteudo')
    <style>
        .space{
            margin-bottom: 2%;
        }

        select{
            height: 32px!important;
        }

        #titleImage{
            display: none;
            padding: 15px;
        }

        #titleRel{
            display: none;
        }

        @media print {
            select{
                border: none!important;
                background: white;
                -webkit-appearance: none;
                -moz-appearance: none;
                text-indent: 1px;
            }

            #btnImpressao{
                display: none;
            }

            #titleImage{
                display: block;
            }
            #titleText{
                display: none;
            }

            #titleRel{
                display: block;
            }
            @page { margin: 0; }
            body { margin: 1.6cm; }

            .sidebar{
                display: none;
            }

            #cardFilter{
                display: none;
            }

            #cardResult{
                display: block;
                position: fixed;
                top: 0;
                left: 0;
                width: 100%;
            }
        }
    </style>

    <div class="row">
        <div class="col-md-12">
            <div id="cardFilter" class="card">
                <div class="card-header">
                    <div class="col-md-12">
                        <h5 class="card-title">Rotas</h5>
                    </div>
                </div>
                <div id="selector-vue" class="card-body ">
                    <form id="formRota" method="post">
                        @csrf
                        <div class="row">
                            <div class="col-md-4 float-left space">
                                <select onchange="buscaFazenda(this.value)" class="form-control" name="grupo"
                                        id="grupo">
                                    <option value="">Selecione o grupo...</option>
                                    @foreach($grupos AS $gr)
                                        <option value="{{ $gr->grupo }}">{{ $gr->grupo }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-md-4 float-left space">
                                <select onchange="getMediaGrupo(this.value)" class="form-control" name="fazenda"
                                        id="fazenda">
                                    <option value="">Selecione a fazenda...</option>
                                </select>
                            </div>

                            <div class="col-md-2 float-left space">
                                <input onchange="calculaPercent(this.value)" placeholder="%" id="percent"
                                       name="percent" class="form-control peso"
                                       type="text">
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-4 space">
                                <select name="produto" id="produto" class="form-control">
                                    <option value="">Selecione o Produto...</option>
                                    @foreach($produtos AS $prod)
                                        <option value="{{ $prod->produto }}">{{ $prod->produto }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-2 float-left space">
                                <input placeholder="Realizado" id="realizado" required class="form-control peso"
                                       name="realizado"
                                       type="text">
                            </div>

                            <div class="col-md-2 float-left space">
                                <input placeholder="Depósito" id="deposito" required class="form-control peso"
                                       name="deposito" type="text">
                            </div>
                        </div>

                        <div class="col-md-12 float-left space">
                            <button onClick="geraRelatorioRota()" type="button" class="btn btn-success float-right">
                                Gerar Relatório
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div id="cardResult" class="card">
        <div class="card-footer">
            <div class="col-md-12">
                <h5 id="titleText" align="center" class="card-title">
                    Dados do relatório
                </h5>
                <h5 id="titleImage" align="center" class="card-title">
                    <img src="{{ asset('img/logo.png') }}" alt="">
                </h5>
                <hr style="border: 1px solid #ddd">
            </div>

            <div class="col-md-12">
                <table id="tableRota" class="table table-striped">
                    <thead>
                        <tr>
                            <th>Rota</th>
                            <th>Local</th>
                            <th>Média</th>
                            <th>%</th>
                            <th>% da Média</th>
                            <th>Média X Animais</th>
                            <th>Produto</th>
                            <th>Realizado</th>
                            <th>Depósito</th>
                            <th>#</th>
                        </tr>
                    </thead>

                    <tbody>

                    </tbody>
                </table>

                <div class="col-md-3 float-right">
                    <button id="btnImpressao" onclick="window.print()" type="button" class="btn btn-primary float-right">
                        Imprimir
                    </button>
                </div>
            </div>
        </div>
    </div>
@endsection