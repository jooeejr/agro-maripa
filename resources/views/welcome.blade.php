@extends('layouts.app')

@section('content')
    <div style="margin-top: 10vh" class="container">
        <div class="row">
            <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
                <div class="card card-signin my-5">
                    <div class="card-body">
                        <h5 class="card-title text-center">
                            <img width="100%" src="{{ asset('img/logo.png') }}" alt="">
                        </h5>
                        <form method="POST" action="{{ route('login') }}" class="form-signin">
                            @csrf
                            <div class="form-label-group">
                                <input autocomplete="off" name="email" type="email" id="inputEmail" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="Email address" value="{{ old('email') }}" required autofocus>
                                <label for="inputEmail">Email</label>

                                @if ($errors->has('email'))
                                    <span style="margin-left: 22px;" class="invalid-feedback" role="alert">
                                        <strong>Email ou senha inválidos !</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-label-group">
                                <input autocomplete="off" name="password" type="password" id="inputPassword" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="Password" required>
                                <label for="inputPassword">Senha</label>

                                @if ($errors->has('password'))
                                    <span style="margin-left: 22px;" class="invalid-feedback" role="alert">
                                        <strong>Email ou senha inválidos !</strong>
                                    </span>
                                @endif
                            </div>
                            <button class="btn btn-lg btn-primary btn-block text-uppercase" type="submit">Entrar</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

