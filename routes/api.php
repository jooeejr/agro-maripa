<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Busca os lotes de um determinado local
Route::post('/lotes', 'LoteController@getLote');

// Busca as fazendas de um determinado grupo
Route::post('/fazendas', 'LoteController@getFazenda');

// Busca os lotes de um determinado local
Route::post('/lotes-consumo', 'LoteController@getLoteConsumo');

// Busca a media de peso de um determinado lote
Route::post('/peso-lotes', 'LoteController@getMedia');


// Busca a media de peso de um determinado lote
Route::post('/peso-grupo', 'LoteController@getMediaGrupo');

