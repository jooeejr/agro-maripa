<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Página Inicial
Route::get('/', function () {
    return view('welcome');
});

// Autenticação para todas as rotas
Auth::routes();

// Dashboard
Route::get('/home', 'DashboardController@index')->name('dashboard');

// Busca os dados para a dashboard de uma localidade
Route::post('/dashboard-local', 'DashboardController@getDashboardFilter');


// Página de Produtos
Route::get('/produtos', 'ProdutoController@index')->name('form.produto');

// Post de cadastro de produto
Route::post('/produtos', 'ProdutoController@store')->name('new.produto');

// Formulário de Atualização
Route::get('/produtos/{id}', 'ProdutoController@edit')->name('form.edit.produto');

// Atualiza um produto
Route::patch('/produtos/{id}', 'ProdutoController@update')->name('update.produto');

// Deleta um produto
Route::delete('/produtos/{id}', 'ProdutoController@destroy')->name('delete.produto');

// Lançamento de um novo lote do produto
Route::get('/lancamento-produto/{id}', 'ProdutoController@indexLancamento')->name('form.lancamento.produto');

// Cadastra o lançamento de um produto
Route::post('/lancamento-produto/{id}', 'ProdutoController@storeLancamento')->name('new.lancamento.produto');



// Página de Fornecedores
Route::get('/fornecedores', 'FornecedorController@index')->name('form.fornecedor');

// Post para cadastro do fornecedor
Route::post('/fornecedores', 'FornecedorController@store')->name('new.fornecedor');

// Formulário de Atualização
Route::get('/fornecedores/{id}', 'FornecedorController@edit')->name('form.edit.fornecedor');

// Atualiza um fornecedor
Route::patch('/fornecedores/{id}', 'FornecedorController@update')->name('update.fornecedor');

// Deleta um fornecedor
Route::delete('/fornecedores/{id}', 'FornecedorController@destroy')->name('delete.fornecedor');




// Página de Lotes
Route::get('/lotes', 'LoteController@index')->name('form.lote');

// Cadastro de lote
Route::post('/lotes', 'LoteController@uploadFile')->name('new.lote');

// Formulario de edição
Route::get('/lotes/{id}', 'LoteController@edit')->name('form.edit.lote');

// Update do Lote
Route::patch('/lotes/{id}', 'LoteController@update')->name('update.lote');

// Deleta um lote
Route::delete('/lotes/{id}', 'LoteController@destroy')->name('delete.lote');

// Lotes cadastrados
Route::get('/dados-lotes', 'LoteController@dadosLotes')->name('data.lote');




// Página de Lançamentos
Route::get('/lancamentos', 'LancamentoController@index')->name('form.lancamento');

// Realização do lançamento
Route::post('/lancamentos', 'LancamentoController@store')->name('new.lancamento');

// Dados cadastrados no lancamentos
Route::get('/dados-lancamentos', 'LancamentoController@dadosLancamentos')->name('dados.lancamento');




// Página de Rações
Route::get('/racoes', 'RacaoController@index')->name('form.racao');

// Cadastro de uma ração
Route::post('/racoes', 'RacaoController@store')->name('new.racao');

// Formulário de Atualização de uma ração
Route::get('/racoes/{id}', 'RacaoController@edit')->name('form.edit.racao');

// Atualização da Ração
Route::patch('/racoes/{id}', 'RacaoController@update')->name('update.racao');

// Delete de uma ração
Route::delete('/racoes/{id}', 'RacaoController@destroy')->name('delete.racao');


// Página de montagem da ração
Route::get('/montar-racao/{id}', 'RacaoController@indexMontar')->name('form.montarRacao');

// Cadastro dos produtos na ração
Route::post('/montar-racao/{id}', 'RacaoController@storeMontar')->name('new.montarRacao');

// Delete do produto na ração
Route::delete('/montar-racao/{id}', 'RacaoController@destroyMontar')->name('delete.montarRacao');




// Página de Relatório de consumo
Route::get('/consumo', 'ConsumoController@index')->name('form.consumo');

// Página de Relatório de consumo
Route::post('/consumo', 'ConsumoController@getConsumo')->name('get.consumo');

// Página de Relatório de consumo
Route::post('/consumo', 'ConsumoController@getConsumo')->name('get.consumo');


// Página de Rotas
Route::get('/rota', 'RotaController@index')->name('form.rota');

// Relatório de rotas
Route::post('/relatorio-rota', 'RotaController@getRelatorio')->name('get.relatorio.rota');



// Página de Relatório de ganho de peso
Route::get('/peso', 'GanhoPesoController@previa')->name('form.peso');


